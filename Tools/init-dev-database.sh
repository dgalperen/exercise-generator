#!/bin/sh

export DB_HOST=localhost
export DB_NAME=exercisegenerator
export DB_USER=exercisegenerator
export DB_PASSWORD=RT3tVKuPvUi0JMHy
export PGPASSWORD=RT3tVKuPvUi0JMHy

# Create database
echo "Creating database..."
dropdb --if-exists -h ${DB_HOST} -U ${DB_USER} ${DB_NAME}
createdb -h ${DB_HOST} -U ${DB_USER} ${DB_NAME}
echo "Done."

# Execute flyway migration for master server
echo "Executing Flyway migration..."
cd ../backend/server
mvn flyway:migrate -Dflyway.url=jdbc:postgresql://${DB_HOST}:5432/${DB_NAME} -Dflyway.user=${DB_USER} -Dflyway.password=${DB_PASSWORD} --quiet
cd - > /dev/null
echo "Done."
