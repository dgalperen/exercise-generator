package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Log4j2
public class IEEEToDecimalTest {

    @Test
    public void removeExponentFromNumberTest() {
        assertEquals( "11001,1", IEEEToDecimal.removeExponentFromNumber("110,011", 2));
        assertEquals( "11001", IEEEToDecimal.removeExponentFromNumber("110,010", 2));
        assertEquals( "11001000", IEEEToDecimal.removeExponentFromNumber("110,010", 5));

        assertEquals( "1,10011", IEEEToDecimal.removeExponentFromNumber("110,011", -2));
        assertEquals( "0,0110011", IEEEToDecimal.removeExponentFromNumber("110,011", -4));
        assertEquals( "0,00110011", IEEEToDecimal.removeExponentFromNumber("110,011", -5));
        assertEquals( "1,1", IEEEToDecimal.removeExponentFromNumber("110,000", -2));
    }

}