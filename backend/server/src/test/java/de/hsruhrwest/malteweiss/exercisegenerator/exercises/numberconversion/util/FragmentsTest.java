package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.fragments.Fragments;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Log4j2
public class FragmentsTest {
    @Test
    public void createAdditionTable() {
        var result = Fragments.createAdditionComputationTable("1101", "1111", "+", NumberBase.BINARY);

        assertEquals(" 1101", result.getSummand1());
        assertEquals(" 1111", result.getSummand2());
        assertEquals("1111 ", result.getCarryOver());
        assertEquals("11100", result.getResult());
    }
}
