package de.hsruhrwest.malteweiss.exercisegenerator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.hsruhrwest.malteweiss.exercisegenerator.Application;
import de.hsruhrwest.malteweiss.exercisegenerator.controller.response.ExerciseGroupListTO;
import de.hsruhrwest.malteweiss.exercisegenerator.controller.response.ExerciseResponseTO;
import de.hsruhrwest.malteweiss.exercisegenerator.services.ExerciseService;
import de.hsruhrwest.malteweiss.exercisegenerator.services.model.DescribedExerciseTO;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@Log4j2
public class ExerciseControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ExerciseService exerciseService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testGetExerciseGroups() throws Exception {
        String result =
            mockMvc.perform(
                    get("/api/exercise-groups")
            ).andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString();

        // Get all groups
        ExerciseGroupListTO exerciseGroupListTO = objectMapper.readValue(result, ExerciseGroupListTO.class);

        // Extract exercises
        List<DescribedExerciseTO> exercises = exerciseGroupListTO.getGroups().stream()
                .flatMap(group -> group.getExercises().stream())
                .collect(Collectors.toList());

        assertNotNull(exercises);
        assertTrue(exercises.size() > 0);
    }

    @Test
    public void testGetExercisesAndSolutions() throws Exception {

        var exercises =
                exerciseService.getAvailableExerciseGroups().stream()
                        .flatMap(group -> group.getExercises().stream())
                        .collect(Collectors.toList());

        Set<String> exerciseIds = new HashSet<>();

        for(var exercise : exercises) {
            // Ensure that ID of this exercise has not been used in other exercise
            if(exerciseIds.contains(exercise.getId())) {
                fail("ID '" + exercise.getId() + "' is used for multiple exercise. IDs must be unique.");
            }
            exerciseIds.add(exercise.getId());

            // Get exercise
            String apiRequest = String.format("/api/exercise/%s", exercise.getId());
            String responseString = mockMvc.perform(get(apiRequest))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse().getContentAsString();
            assertNotNull(responseString);
            ExerciseResponseTO exerciseResponse = objectMapper.readValue(responseString, ExerciseResponseTO.class);
            assertNotNull(exerciseResponse);
            assertNotNull(exerciseResponse.getHtmlQuestion());
            assertNotNull(exerciseResponse.getHtmlSolution());

            log.info("Exercise and solution for " + exercise.getName() + " is present.");
        }
    }
}