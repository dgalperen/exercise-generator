package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Log4j2
public class DecimalToIEEETest {

    @Test
    public void normalizeBinaryFloatNumberTest() {
        {
            DecimalToIEEE.NormalizedFloatResult result = DecimalToIEEE.normalizeBinaryFloatNumber("1.0");
            assertEquals("1", result.getNormalizedNumber());
            assertEquals(0, result.getExponent());
        }

        {
            DecimalToIEEE.NormalizedFloatResult result = DecimalToIEEE.normalizeBinaryFloatNumber("1000");
            assertEquals("1", result.getNormalizedNumber());
            assertEquals(3, result.getExponent());
        }
        {
            DecimalToIEEE.NormalizedFloatResult result = DecimalToIEEE.normalizeBinaryFloatNumber("1000.01");
            assertEquals("1,00001", result.getNormalizedNumber());
            assertEquals(3, result.getExponent());
        }

        {
            DecimalToIEEE.NormalizedFloatResult result = DecimalToIEEE.normalizeBinaryFloatNumber("0.1");
            assertEquals("1", result.getNormalizedNumber());
            assertEquals(-1, result.getExponent());
        }
        {
            DecimalToIEEE.NormalizedFloatResult result = DecimalToIEEE.normalizeBinaryFloatNumber("0.0101");
            assertEquals("1,01", result.getNormalizedNumber());
            assertEquals(-2, result.getExponent());
        }
    }

}