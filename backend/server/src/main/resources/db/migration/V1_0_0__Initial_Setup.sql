--
-- Exercise access
--
CREATE TABLE exercise_access_log_entry (
    id BIGSERIAL PRIMARY KEY,
    created timestamp NOT NULL DEFAULT NOW(),
    exercise_id varchar(256) NOT NULL
);
