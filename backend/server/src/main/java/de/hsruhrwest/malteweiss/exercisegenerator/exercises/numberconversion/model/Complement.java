package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model;

import lombok.Getter;

@Getter
public enum Complement {
    ONE, TWO
}
