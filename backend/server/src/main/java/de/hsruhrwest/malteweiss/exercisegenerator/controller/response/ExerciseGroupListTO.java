package de.hsruhrwest.malteweiss.exercisegenerator.controller.response;

import de.hsruhrwest.malteweiss.exercisegenerator.services.model.ExerciseGroupTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/** Definition of exercise group */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExerciseGroupListTO {
    /**
     * List of exercise groups
     */
    private List<ExerciseGroupTO> groups;
}