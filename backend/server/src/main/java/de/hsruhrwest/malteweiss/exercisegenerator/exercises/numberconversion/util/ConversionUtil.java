package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

@Log4j2
public class ConversionUtil {
    /**
     * Converts a number of a string in a given base.
     * @param number number
     * @param base base
     * @return string
     */
    public static String numberToStringInBase(long number, NumberBase base) {
        switch(base) {
            case BINARY:
                return Long.toBinaryString(number);
            case OCTAL:
                return Long.toOctalString(number);
            case HEXADECIMAL:
                return Long.toHexString(number).toUpperCase();
            case DECIMAL:
                return String.valueOf(number);
            default:
                throw new IllegalStateException("Unknown base: " + base);
        }
    }

    /**
     * Converts a binary, octal, hexidecimal digit to a number.
     * @param digit
     * @return numeric digit
     */
    public static int digitToDecimal(int digit) {
        switch(digit) {
            case 'A': return 10;
            case 'B': return 11;
            case 'C': return 12;
            case 'D': return 13;
            case 'E': return 14;
            case 'F': return 15;
            default: return digit - '0';
        }
    }

    /**
     * Converts a double value into a Human-readable format by removing ending
     * zeros (and a potentially remaining comma).
     * @param value value
     * @return Human-readable string
     */
    public static String toReadableNumberString(BigDecimal value) {
        return toReadableNumberString(value, 0);
    }

    /**
     * Converts a number string in German locale to BigDecimal
     * @param value string value
     * @return BigDecimal number
     */
    public static BigDecimal germanStringToBigDecimal(String value) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        decimalFormat.setGroupingUsed(false);
        decimalFormat.setParseBigDecimal(true);
        try {
            return (BigDecimal) decimalFormat.parse(value);
        } catch (ParseException e) {
            log.error(e);
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Converts a double value into a Human-readable format by removing ending
     * zeros (and a potentially remaining comma).
     * @param value value
     * @param minimumFractionDigits minimum number of fraction digits
     * @return Human-readable string
     */
    public static String toReadableNumberString(BigDecimal value, int minimumFractionDigits) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        decimalFormat.setMaximumFractionDigits(value.toString().length());
        decimalFormat.setMinimumFractionDigits(minimumFractionDigits);
        decimalFormat.setGroupingUsed(false);
        return decimalFormat.format(value);
    }

    /**
     * Converts a numeric digit to a binary/octal/hexidecimal character.
     * @param decimal decimal
     * @return character
     */
    public static int decimalToDigit(int decimal) {
        switch(decimal) {
            case 10: return 'A';
            case 11: return 'B';
            case 12: return 'C';
            case 13: return 'D';
            case 14: return 'E';
            case 15: return 'F';
            default: return '0' + decimal;
        }
    }
}
