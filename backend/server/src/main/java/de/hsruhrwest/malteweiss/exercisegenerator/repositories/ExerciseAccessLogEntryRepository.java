package de.hsruhrwest.malteweiss.exercisegenerator.repositories;

import de.hsruhrwest.malteweiss.exercisegenerator.model.database.ExerciseAccessLogEntry;
import org.springframework.data.repository.CrudRepository;

public interface ExerciseAccessLogEntryRepository extends CrudRepository<ExerciseAccessLogEntry, Long> {
    /**
     * Counts number of access entries by exercise ID.
     * @param exerciseId exercise ID
     * @return number of access entries
     */
    long countByExerciseId(String exerciseId);
}
