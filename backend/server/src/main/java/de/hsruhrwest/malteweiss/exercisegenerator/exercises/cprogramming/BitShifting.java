package de.hsruhrwest.malteweiss.exercisegenerator.exercises.cprogramming;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.StringUtil;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;


@Log4j2
@Exercise(
        id = "BitShifting"
)
public class BitShifting extends AbstractExerciseGenerator {
    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private int leftOperand;
        private int rightOperand;
        private String operator;
    }

    public BitShifting () {
        super(Model.class, "Bitweises Verschieben", "CProgramming/BitShifting");
    }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // create random operands
        int leftOperand = randomDataGenerator.nextInt(1, 31);
        int rightOperand = randomDataGenerator.nextInt(1, 3);

        // randomly determine which bitwise operator will be used
        String operator =
                switch (randomDataGenerator.nextInt(0,1)) {
                    case 0 -> "<<";
                    default -> ">>";
                };

        // create question text
        Context ctx = new Context();
        ctx.setVariable("leftOperand", leftOperand);
        ctx.setVariable("leftOperandBinary", Integer.toBinaryString(leftOperand));
        ctx.setVariable("rightOperand", rightOperand);
        ctx.setVariable("rightOperandBinary", Integer.toBinaryString(rightOperand));
        ctx.setVariable("operator", operator);

        var question = processQuestionTemplate(templateEngine, ctx);

        // create model
        Model model = Model.builder()
                .leftOperand(leftOperand)
                .rightOperand(rightOperand)
                .operator(operator)
                .build();

        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        // left operands * right operands * two operators
        return 31*3*2;
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        Model model = (Model) modelObject;

        // left operand converted to binary
        String leftOperandBinary = Integer.toBinaryString(model.getLeftOperand());

        // find out how many bits it takes to represent leftOperand
        int numberOfBits = (int) (Math.log(model.getLeftOperand()) / Math.log(2)) + 1;

        // zeros to fill up 8 bit number
        String leftOperandZeros = StringUtil.decimalToBinaryStringFixedLength(0, 8 - numberOfBits);

        // create solution based on randomized operator
        String resultBinary = "";
        String resultZerosLeft = "";
        String resultZerosRight = "";
        int result = 0;
        switch (model.getOperator()){
            case "<<":
                // result as integer
                result = model.getLeftOperand() << model.getRightOperand();
                // result as binary strings
                resultZerosLeft = StringUtil.decimalToBinaryStringFixedLength(0, 8 - numberOfBits - model.getRightOperand());
                resultBinary = leftOperandBinary;
                resultZerosRight = StringUtil.decimalToBinaryStringFixedLength(0, model.getRightOperand());
                break;
            case ">>":
                //result as integer
                result = model.getLeftOperand() >> model.getRightOperand();
                // result as binary strings
                resultZerosLeft = StringUtil.decimalToBinaryStringFixedLength(0, Math.min(8, 8 - (numberOfBits - model.getRightOperand())));
                resultBinary = leftOperandBinary.substring(0,Math.max(0, numberOfBits - model.getRightOperand()));
                break;
            default:
                throw new IllegalStateException("Unknown operator: " + model.getOperator());
        }

        // create solution text
        Context ctx = new Context();
        ctx.setVariable("leftOperand", model.getLeftOperand());
        ctx.setVariable("rightOperand", model.getRightOperand());
        ctx.setVariable("operator", model.getOperator());
        ctx.setVariable("leftOperandBinary", leftOperandBinary);
        ctx.setVariable("leftOperandZeros",leftOperandZeros);
        ctx.setVariable("result", result);
        ctx.setVariable("resultBinary", resultBinary);
        ctx.setVariable("resultZerosLeft",resultZerosLeft);
        ctx.setVariable("resultZerosRight",resultZerosRight);

        return processSolutionTemplate(templateEngine, ctx);
    }
}
