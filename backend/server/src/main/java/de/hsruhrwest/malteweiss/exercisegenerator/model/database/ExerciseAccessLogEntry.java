package de.hsruhrwest.malteweiss.exercisegenerator.model.database;

import jakarta.persistence.*;
import lombok.Builder;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Entity
@Builder
public class ExerciseAccessLogEntry {
    /**
     * ID of this order which is represented by the hash of the nesting request.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Creation time.
     */
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime created;

    /**
     * ID of called exercise.
     */
    private String exerciseId;
}
