package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Exercise(id = "BinOctHexIntegerToInteger")
public class BinOctHexIntegerToInteger extends BinOctHexIntegerToDecimal {

    /**
     * Constructs object.
     */
    public BinOctHexIntegerToInteger() {
        super(false, "Binäre/oktale/hexadezimale Ganzzahl &rarr; dezimale Darstellung");
    }

}
