package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;

import java.util.HashMap;
import java.util.Map;

@Exercise(id = "KVDiagramMinTerm")
public class MinTermKVDiagramExercise extends AbstractKVDiagramExercise {
    /**
     * Default constructor setting name of exercise.
     */
    public MinTermKVDiagramExercise() {
        super("Aussagenlogische Formel mit der Minterm-Methode vereinfachen");
    }

    @Override
    protected boolean getCoverValue() {
        return true;    // 1
    }

    @Override
    protected boolean isRelevantRow(boolean truthTableValue) {
        return truthTableValue;
    }

    @Override
    protected String getParameterPrefixForLetter(boolean parameterValue) {
        return parameterValue == false ? "&#xAC;" : "";
    }

    @Override
    protected String getSolutionInnerOperatorHtml() {
        return "&and;";
    }

    @Override
    protected String getSolutionOuterOperatorHtml() {
        return "&or;";
    }

    @Override
    protected String getExpressionOutput(String configMinTermExpression) {
        return configMinTermExpression
                .replace("-", "&#xAC;") // negation
                .replace(",", " " + getSolutionInnerOperatorHtml() + " ");
    }

    @Override
    protected Map<String, String> getLocalizedWords() {
        var localization = new HashMap<String, String>();
        localization.put("MethodName", "Minterm");
        localization.put("CoveredElementsName", "Einsen");
        localization.put("JoinOperator", "Oder-Operator (v)");
        localization.put("NormFormTermAcc", "Disjunktiver Normalform (DNF)");
        return localization;
    }
}
