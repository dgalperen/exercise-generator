package de.hsruhrwest.malteweiss.exercisegenerator.services;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.services.model.ExerciseGroupTO;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

public interface ExerciseService {
    /**
     * Returns list of all enabled exercise generators.
     * @return list of generators
     * @throws IOException if groups cannot be loaded
     */
    List<ExerciseGroupTO> getAvailableExerciseGroups() throws IOException;

    /**
     * Returns list of all exercise groups.
     * @return list of generators
     */
    List<AbstractExerciseGenerator> getAvailableExerciseGenerators() throws IOException;

    /**
     * Returns exercise generator by ID.
     * @param id ID
     * @return exercise generator
     * @throws NoSuchElementException if generator is not found
     */
    AbstractExerciseGenerator getExerciseGenerator(String id) throws NoSuchElementException;

    /**
     * Logs access to exercise.
     * @param generator generator for exercise
     */
    void logExerciseAccess(AbstractExerciseGenerator generator);

    /**
     * Returns number of accesses for a given exercise.
     * @param generator generator for exercise
     */
    long getExerciseAccessCount(AbstractExerciseGenerator generator);
}