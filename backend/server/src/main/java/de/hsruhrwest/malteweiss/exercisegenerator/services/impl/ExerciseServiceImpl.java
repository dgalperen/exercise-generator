package de.hsruhrwest.malteweiss.exercisegenerator.services.impl;

import com.esotericsoftware.yamlbeans.YamlReader;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.model.database.ExerciseAccessLogEntry;
import de.hsruhrwest.malteweiss.exercisegenerator.repositories.ExerciseAccessLogEntryRepository;
import de.hsruhrwest.malteweiss.exercisegenerator.services.ExerciseService;
import de.hsruhrwest.malteweiss.exercisegenerator.services.model.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
public class ExerciseServiceImpl implements ExerciseService {

    private static final String EXERCISE_GROUPS_RESOURCES = "exercise-groups.yaml";

    @Autowired
    private ExerciseAccessLogEntryRepository exerciseAccessLogEntryRepository;

    @Override
    public List<ExerciseGroupTO> getAvailableExerciseGroups() throws IOException {
        List<AbstractExerciseGenerator> allGenerators = getAvailableExerciseGenerators();

        // Read exercise group configuration
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(getClass().getClassLoader().getResource(EXERCISE_GROUPS_RESOURCES).openStream(), StandardCharsets.UTF_8))) {
            // Read configuration
            YamlReader reader = new YamlReader(br);
            ConfigExerciseGroupListTO configGroupList = reader.read(ConfigExerciseGroupListTO.class);
            reader.close();

            // Transform list of configured exercise groups to a list with described exercise (with name)
            List<ExerciseGroupTO> exerciseGroups = new ArrayList<>();
            for(ConfigExerciseGroupTO groupTO : configGroupList.getGroups()) {
                List<DescribedExerciseTO> describedExercises = new ArrayList<>();
                for(ExerciseConfigTO exerciseConfig : groupTO.getExercises()) {
                    // Get generator for this class
                    AbstractExerciseGenerator generator =
                        allGenerators.stream()
                            .filter(g ->
                                    g.getClass().getAnnotation(Exercise.class).id().equals(exerciseConfig.getClassId()) &&
                                    Objects.equals(g.getParams(), exerciseConfig.getParams()))
                            .findAny()
                            .orElseThrow(() -> new NoSuchElementException("No enabled generator with ID '" + exerciseConfig.getClassId() + "' found."));

                    // Get name of this exercise
                    Exercise exercise = generator.getClass().getAnnotation(Exercise.class);
                    if(exercise.enabled()) {
                        DescribedExerciseTO describedExerciseTO =
                            DescribedExerciseTO.builder()
                                    .id(generator.getId())
                                    .name(generator.getName())
                                    .exam(exerciseConfig.getExam())
                                    .numVariants(generator.getNumberOfVariants())
                                    .numViews(getExerciseAccessCount(generator))
                                    .build();

                        describedExercises.add(describedExerciseTO);
                    }
                }

                exerciseGroups.add(new ExerciseGroupTO(groupTO.getName(), describedExercises));
            }
            return exerciseGroups;
        }
    }

    @Override
    @Cacheable("availableExercises")
    public List<AbstractExerciseGenerator> getAvailableExerciseGenerators() {
        // Create map from exercise IDs to classes
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Exercise.class));

        var exerciseIdsToClasses = new HashMap<String, Class>();
        for (BeanDefinition bd : scanner.findCandidateComponents(AbstractExerciseGenerator.class.getPackageName())) {
            Class<?> cls;
            try {
                cls = Class.forName(bd.getBeanClassName());

                if(!cls.getAnnotation(Exercise.class).enabled()) {
                    log.info("Exercise " + bd.getBeanClassName() + " is disabled. Skipping.");
                }

                exerciseIdsToClasses.put(cls.getAnnotation(Exercise.class).id(), cls);
            } catch (ClassNotFoundException e) {
                log.error(e);
                continue;
            }
        }

        // Read exercise group configuration and get generators
        List<AbstractExerciseGenerator> exerciseGenerators = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(getClass().getClassLoader().getResource(EXERCISE_GROUPS_RESOURCES).openStream(), StandardCharsets.UTF_8))) {

            // Read configuration
            YamlReader reader = new YamlReader(br);
            ConfigExerciseGroupListTO configGroupList = reader.read(ConfigExerciseGroupListTO.class);
            for(var exerciseConfigTO : configGroupList.getGroups().stream().flatMap(group -> group.getExercises().stream()).collect(Collectors.toList())) {
                Class cls = exerciseIdsToClasses.get(exerciseConfigTO.getClassId());
                if(cls == null) {
                    log.error("Exercise class " + cls + " not found.");
                    throw new IllegalStateException("Exercise class " + cls + " named in configuration but not found.");
                }

                // Create instance
                try {
                    AbstractExerciseGenerator generator = (AbstractExerciseGenerator) cls.getConstructor().newInstance();
                    // If ID is set use this, otherwise use classID for identification
                    generator.setId(exerciseConfigTO.getId() != null ? exerciseConfigTO.getId() : exerciseConfigTO.getClassId());
                    generator.setExam(exerciseConfigTO.getExam());
                    generator.setParams(exerciseConfigTO.getParams());
                    exerciseGenerators.add(generator);
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    log.error("Cannot execute constructor for exercise class " + cls + ".");
                    log.error(e);
                    throw new IllegalStateException("Exercise class " + cls + " named in configuration but not found.", e);
                }
            }

            reader.close();
        } catch (IOException e) {
            log.error(e);
            throw new IllegalStateException(e);
        }

        return exerciseGenerators;
    }

    @Override
    public AbstractExerciseGenerator getExerciseGenerator(String id) throws NoSuchElementException {
        // Find generator by exercise ID and params hash
        return getAvailableExerciseGenerators().stream()
                .filter(generator -> id.equals(generator.getId()))
                .findAny()
                .get();
    }

    @Override
    public void logExerciseAccess(AbstractExerciseGenerator generator) {
        exerciseAccessLogEntryRepository.save(
                ExerciseAccessLogEntry.builder()
                        .exerciseId(generator.getId())
                        .build()
        );
    }

    @Override
    public long getExerciseAccessCount(AbstractExerciseGenerator generator) {
        return exerciseAccessLogEntryRepository.countByExerciseId(generator.getId());
    }
}
