package de.hsruhrwest.malteweiss.exercisegenerator.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExerciseTO {

    /**
     * Description of exercise.
     */
    private String htmlDescription;

    /**
     * Exercise configuration. Required to generate solution.
     */
    private Object model;

}
