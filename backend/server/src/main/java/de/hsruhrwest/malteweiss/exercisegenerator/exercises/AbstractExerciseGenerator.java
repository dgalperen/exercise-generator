package de.hsruhrwest.malteweiss.exercisegenerator.exercises;

import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.util.Map;

/**
 * Abstract super class for all exercise.
 */
@Log4j2
public abstract class AbstractExerciseGenerator {

    /**
     * Name of template.
     */
    private String templateName;

    /**
     * Constructor that sets model class.
     * @param modelClass model class
     * @param name name of exercise
     * @param templateName template name
     */
    public AbstractExerciseGenerator(Class modelClass, String name, String templateName) {
        this.modelClass = modelClass;
        this.name = name;
        this.templateName = templateName;
    }

    /**
     * Constructor that sets model class.
     * @param modelClass model class
     * @param name name of exercise
     */
    public AbstractExerciseGenerator(Class modelClass, String name) {
        this.modelClass = modelClass;
        this.name = name;
        this.templateName = getClass().getSimpleName();
    }

    /**
     * Returns exercise.
     * @param templateEngine template engine
     * @return exercise as hash-map
     */
    public abstract ExerciseTO createExercise(SpringTemplateEngine templateEngine);

    /**
     * Processes number of variants in an exercise and returns number
     * @return processed number
     */
    public abstract long getNumberOfVariants();

    /**
     * Returns solution for a given exercise description.
     * @param templateEngine template engine
     * @param modelObject exercise model, can be mapped to model class that is returned by getModelClass()
     * @return exercise as string
     */
    public abstract String createSolution(SpringTemplateEngine templateEngine, Object modelObject);

    /**
     * Returns name of template. By default, this is the simple name of the class.
     * @return template name
     */
    protected String getTemplateName() {
        return templateName;
    }

    /**
     * Processes question template and returns result.
     * @param templateEngine template engine
     * @param context context
     * @return resulting string
     */
    protected String processQuestionTemplate(SpringTemplateEngine templateEngine, Context context) {
        return templateEngine.process(getTemplateName() + "/question", context);
    }

    /**
     * Processes solution template and returns result.
     * @param templateEngine template engine
     * @param context context
     * @return resulting string
     */
    protected String processSolutionTemplate(SpringTemplateEngine templateEngine, Context context) {
        return templateEngine.process(getTemplateName() + "/solution", context);
    }

    /**
     * ID of generator.
     */
    @Getter
    @Setter
    private String id;

    /**
     * Class of data model.
     */
    @Getter
    private Class modelClass;

    /**
     * Name of exercise
     */
    @Getter
    private String name;

    /**
     * Exam that exercise was taken from
     */
    @Getter
    @Setter
    private String exam;

    /**
     * Optional configuration
     */
    @Getter
    @Setter
    private Map<String, Object> params;

    /**
     * Returns string value for key
     * @param key key
     * @return string parameter
     */
    public String getStringParam(String key) {
        return (String) params.get(key);
    }
}
