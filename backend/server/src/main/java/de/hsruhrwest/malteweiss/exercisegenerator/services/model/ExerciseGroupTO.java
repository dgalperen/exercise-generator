package de.hsruhrwest.malteweiss.exercisegenerator.services.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/** Definition of exercise group */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExerciseGroupTO {
    /** Name of group */
    private String name;

    /** List of classes */
    private List<DescribedExerciseTO> exercises;
}