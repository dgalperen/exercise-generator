package de.hsruhrwest.malteweiss.exercisegenerator.services.model;

import lombok.Data;

import java.util.List;

@Data
public class ConfigExerciseGroupListTO {
    /** Exercise groups */
    private List<ConfigExerciseGroupTO> groups;
}
