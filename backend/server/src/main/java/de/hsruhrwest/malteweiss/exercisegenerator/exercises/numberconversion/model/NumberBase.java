package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model;

import lombok.Getter;

@Getter
public enum NumberBase {
    BINARY(2), OCTAL(8), DECIMAL(10), HEXADECIMAL(16);

    private int numericBase;

    NumberBase(int numericBase) {
        this.numericBase = numericBase;
    }
}
