package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.fragments.Fragments;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import static de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.LocaleConstants.DECIMAL_SEPARATOR;

@Log4j2
@Exercise(id = "DecimalToBinOctHex")
public class DecimalToBinOctHex extends AbstractExerciseGenerator {

    /**
     * If this attribute is true, numbers with decimals are created.
     */
    @Setter
    private boolean enableDecimals;

    /**
     * Maximum expected digits after comma for solution.
     */
    private static final int MAX_SOLUTION_DIGITS = 10;

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private String number;
        private NumberBase base;
        private int maxSolutionDigits;
    }

    /**
     * Constructs object with enabled decimals.
     */
    public DecimalToBinOctHex() {
        this(true, "Kommazahl &rarr; binäre/oktale/hexadezimale Darstellung");
    }

    /**
     * Constructs object and sets flags.
     * @param enableDecimals determines whether decimals should be created.
     * @param name name of exercise
     */
    public DecimalToBinOctHex(boolean enableDecimals, String name) {
        super(Model.class, name);
        this.enableDecimals = enableDecimals;
    }

    @Override
    protected String getTemplateName() {
        return "DecimalToBinOctHex";
    }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // Create non-decimal random target base
        NumberBase base;
        do {
            base = NumberBase.values()[randomDataGenerator.nextInt(0, NumberBase.values().length - 1)];
        } while(base == NumberBase.DECIMAL);

        // Create random integer number
        String integerPart = String.format("%d", randomDataGenerator.nextLong(1000, 9999));
        String decimalPart = enableDecimals ? String.format("%d", randomDataGenerator.nextLong(1, 20)) : null;

        // Create output system name
        String targetBaseName;
        switch(base) {
            case BINARY:
                targetBaseName = "Binärsystem";
                break;
            case OCTAL:
                targetBaseName = "Oktalsystem";
                break;
            case HEXADECIMAL:
                targetBaseName = "Hexadezimalsystem";
                break;
            default:
                throw new IllegalStateException("Unknown base: " + base);
        }

        String numberString;
        if(decimalPart != null) {
            numberString = String.format("%s%s%s", integerPart, DECIMAL_SEPARATOR, decimalPart);
        } else {
            numberString = String.format("%s", integerPart);
        }

        // Create question text
        Context ctx = new Context();
        ctx.setVariable("number", numberString);
        ctx.setVariable("targetBaseName", targetBaseName);
        ctx.setVariable("enableDecimals", enableDecimals);
        ctx.setVariable("maxDigits", MAX_SOLUTION_DIGITS);

        var question = processQuestionTemplate(templateEngine, ctx);

        // Create model
        var model = Model.builder()
            .number(numberString)
            .base(base)
            .build();

        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        long variants = 3 * (9999 - 1000 + 1);
        if(enableDecimals) {
            variants *= 20;
        }
        return variants;
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        Model model = (Model) modelObject;

        Context ctx = new Context();
        ctx.setVariable("conversionResult",
                Fragments.createDecimalToBinOctHexConversion(model.getNumber(), model.getBase(), MAX_SOLUTION_DIGITS)
        );

        return processSolutionTemplate(templateEngine, ctx);
    }
}
