package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model;

import lombok.Data;

import java.util.List;

/**
 * Configuration of minterms.
 */
@Data
public class CoverageMintermConfigurationTO {
    /**
     * Potential terms.
     */
    private List<CoverageTermTO> terms;
}
