package de.hsruhrwest.malteweiss.exercisegenerator.controller;

import de.hsruhrwest.malteweiss.exercisegenerator.services.ExerciseService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class ServerStatusController {

    @Value("${server.name}")
    private String serverName;

    @Value("${server.version}")
    private String serverVersion;

    @Autowired
    private ExerciseService exerciseService;

    /**
     * Returns OK (200) if server is up and healthy
     * @return 200 if server is healthy, error value if not
     */
    @GetMapping(value = "/server/health")
    public ResponseEntity<?> getServerHealth() {
        // Sanity check: Check availability of exercise data
        try {
            if(exerciseService.getAvailableExerciseGroups().size() == 0) {
                return ResponseEntity.internalServerError().build();
            }
        } catch (Exception e) {
            log.error(e);
            return ResponseEntity.internalServerError().build();
        }

        // Return positive response
        return new ResponseEntity<>(String.format("%s (version %s) is online.", serverName, serverVersion),
                HttpStatus.OK);
    }

}
