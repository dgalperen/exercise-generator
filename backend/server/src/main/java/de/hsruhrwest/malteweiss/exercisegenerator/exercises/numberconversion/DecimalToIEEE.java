package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.fragments.Fragments;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.ConversionUtil;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.math.BigDecimal;

import static de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.LocaleConstants.DECIMAL_SEPARATOR;
import static de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.StringUtil.leftPadString;
import static de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.StringUtil.rightPadString;

@Log4j2
@Exercise(id = "DecimalToIEEE")
public class DecimalToIEEE extends AbstractExerciseGenerator {

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private BigDecimal number;
    }

    /**
     * Constructs object
     */
    public DecimalToIEEE() { super(Model.class, "Dezimalzahl &rarr; Gleitpunktzahl im IEEE 754/854-Format"); }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // Create 32-bit Number as Binary or Decimal
        BigDecimal number = BigDecimal.valueOf(randomDataGenerator.nextInt(0, 255)).add(BigDecimal.ONE.divide(BigDecimal.valueOf(2).pow(randomDataGenerator.nextInt(1, 6))));
        if(randomDataGenerator.nextInt(1, 100) <= 50) {
            number = number.multiply(BigDecimal.valueOf(-1));
        }

        // Create question text
        Context ctx = new Context();
        ctx.setVariable("number", ConversionUtil.toReadableNumberString(number));

        var question = processQuestionTemplate(templateEngine, ctx);

        // Create model
        var model = Model.builder()
                .number(number)
                .build();

        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        return 256 * 2;
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        // Extract model
        Model model = (Model) modelObject;
        boolean isNegative = model.getNumber().compareTo(BigDecimal.ZERO) < 0;
        BigDecimal absoluteNumber = model.getNumber().abs();

        // Convert given number to binary
        Fragments.DecimalToBinOctHexResult conversionToBinaryResult =
                Fragments.createDecimalToBinOctHexConversion(
                        ConversionUtil.toReadableNumberString(absoluteNumber),
                        NumberBase.BINARY,
                        32);

        // Compute normalized result of float number and retriee exponent
        NormalizedFloatResult normalizationResult = normalizeBinaryFloatNumber(conversionToBinaryResult.getMaxDigitResult().replace(DECIMAL_SEPARATOR, "."));

        // Compute biased exponent
        long biasedExponent = 127 + normalizationResult.getExponent();

        // Setup template
        Context ctx = new Context();
        ctx.setVariable("isNegative", isNegative);
        ctx.setVariable("questionNumber", ConversionUtil.toReadableNumberString(model.getNumber()));
        ctx.setVariable("positiveQuestionNumber", ConversionUtil.toReadableNumberString(absoluteNumber));
        ctx.setVariable("conversionToBinaryResult", conversionToBinaryResult);
        ctx.setVariable("normalizationResult", normalizationResult);
        ctx.setVariable("biasedExponent", biasedExponent);
        ctx.setVariable("biasedExponentBinaryString", leftPadString(Long.toBinaryString(biasedExponent), '0', 8));
        ctx.setVariable("biasedExponentIntegerConversionResult", Fragments.createIntegerConversionTable(biasedExponent, NumberBase.BINARY));
        ctx.setVariable("mantissaBits", rightPadString(normalizationResult.getNormalizedNumberDecimalPart(), '0', 23).substring(0, 23));
        ctx.setVariable("signBit", isNegative ? 1 : 0);
        return processSolutionTemplate(templateEngine, ctx);
    }

    /**
     * Result of a float normalization.
     */
    @AllArgsConstructor
    @Data
    public static class NormalizedFloatResult {
        private String normalizedNumber;
        private String normalizedNumberIntegerPart;
        private String normalizedNumberDecimalPart;
        private long exponent;
    }

    /**
     * Normalized a binary number to the form of "1,..."
     * @param number binary number as string
     * @return result of normalization
     */
    protected static NormalizedFloatResult normalizeBinaryFloatNumber(String number) {
        long exponent = 0;

        BigDecimal result = new BigDecimal(number);
        while(result.compareTo(BigDecimal.ONE) < 0){
            result = result.multiply(BigDecimal.valueOf(10));
            exponent--;
        }
        while(result.compareTo(BigDecimal.valueOf(10)) >= 0){
            result = result.divide(BigDecimal.valueOf(10));
            exponent++;
        }

        String readableNumber = ConversionUtil.toReadableNumberString(result);
        String[] items = readableNumber.split(DECIMAL_SEPARATOR);

        return new NormalizedFloatResult(readableNumber, items[0], items.length > 1 ? items[1] : "0", exponent);
    }
}