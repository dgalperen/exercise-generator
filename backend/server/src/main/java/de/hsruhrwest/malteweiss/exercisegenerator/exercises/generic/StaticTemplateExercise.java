package de.hsruhrwest.malteweiss.exercisegenerator.exercises.generic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.util.List;

@Log4j2
@Exercise(id = "StaticTemplateExercise")
public class StaticTemplateExercise extends AbstractExerciseGenerator {

    private static final String NAME_TEMPLATE_KEY = "name";

    private static final String QUESTION_TEMPLATE_KEY = "question";

    private static final String SOLUTION_TEMPLATE_KEY = "solution";

    private int numberOfVariants;

    /**
     * Constructor that sets model class.
     */
    public StaticTemplateExercise() {
        super(Model.class, "---StaticTemplateExercise---");
    }

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private String solutionTemplate;
    }

    @NoArgsConstructor
    @Data
    public static class StaticTemplateParams {
        private String name;
        private String question;
        private String solution;
        private List<StaticTemplateVariant> variants;
    }

    @NoArgsConstructor
    @Data
    public static class StaticTemplateVariant {
        private String question;
        private String solution;
    }

    @Override
    public String getName() {
        return getStringParam(NAME_TEMPLATE_KEY);
    }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        // Convert generic map to convenient internal model for params
        StaticTemplateParams staticTemplateParams = getStaticTemplateParams();

        // Create question and solution text
        String question;
        String solution;

        // derive question and solution text from variants if variants exist
        if (!CollectionUtils.isEmpty(staticTemplateParams.getVariants())) {
            if (getStringParam(QUESTION_TEMPLATE_KEY) != null || getStringParam(SOLUTION_TEMPLATE_KEY) != null){
                throw new IllegalStateException("Error in configuration: Both question/solution and variants are defined.");
            }
            int randomNumber = new RandomDataGenerator().nextInt(0, staticTemplateParams.getVariants().size() - 1);
            question = staticTemplateParams.getVariants().get(randomNumber).getQuestion();
            solution = staticTemplateParams.getVariants().get(randomNumber).getSolution();
        }
        else {
            question = getStringParam(QUESTION_TEMPLATE_KEY);
            solution = getStringParam(SOLUTION_TEMPLATE_KEY);
        }

        question = templateEngine.process(question, new Context());

        // Create model
        Model model = Model.builder()
                .solutionTemplate(solution)
                .build();

        return new ExerciseTO(question, model);
    }

    /**
     * Returns static template params for this exercise.
     * @return static template params
     */
    private StaticTemplateParams getStaticTemplateParams() {
        StaticTemplateParams staticTemplateParams;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(getParams());
            staticTemplateParams = objectMapper.readValue(json, StaticTemplateParams.class);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
        return staticTemplateParams;
    }

    @Override
    public long getNumberOfVariants() {
        var staticTemplateParams = getStaticTemplateParams();
        // If variants are specified, return their amount
        if (!CollectionUtils.isEmpty(staticTemplateParams.getVariants())) {
            return staticTemplateParams.getVariants().size();
        }
        // Otherwise, only one variant exists
        return 1;
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        return templateEngine.process(((Model) modelObject).getSolutionTemplate(), new Context());
    }

}
