package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.booltabletonf;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Exercise(id = "BoolTableToDNF")
public class BoolTableToDNF extends AbstractBoolTableToNFBase {

    public BoolTableToDNF() {
        super("Wahrheitstabelle in Disjunktive Normalform (DNF) &uuml;berf&uuml;hren");
    }

    @Override
    protected boolean isRelevantRow(boolean truthTableValue) {
        return truthTableValue;
    }

    @Override
    protected String getParameterPrefixForLetter(boolean parameterValue) {
        return parameterValue == false ? "&#xAC;" : "";
    }

    @Override
    protected String getSolutionInnerOperatorHtml() {
        return "&and;";
    }

    @Override
    protected String getSolutionOuterOperatorHtml() {
        return "&or;";
    }

    @Override
    protected String getTemplateTaskText() {
        return "Stellen Sie die gegebene Funktion in der disjunktiven Normalform (DNF) dar.";
    }

    @Override
    protected String getTemplateAnswerText() {
        return "Die disjunktive Normalform (DNF) sieht für die oben gezeigte Wahrheitstabelle wie folgt aus:";
    }
}
