package de.hsruhrwest.malteweiss.exercisegenerator.services.model;

import lombok.Data;

import java.util.Map;

/** Definition exercise */
@Data
public class ExerciseConfigTO {
    /** ID of exercise class */
    private String classId;

    /** ID of exercise (optional) */
    private String id;

    /** Optional exam description indicating in which exam the exercise was used  */
    private String exam;

    /** Optional config */
    private Map<String, Object> params;
}