package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.fragments.Fragments;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.Complement;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import static de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.StringUtil.leftPadString;

@Log4j2
@Exercise(id = "NegativeDecToBinaryComplement")
public class NegativeDecToBinaryComplement extends AbstractExerciseGenerator {

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private Long number;
        private Long bitCount;
        private Complement complement;
    }

    /**
     * Standard constructor.
     */
    public NegativeDecToBinaryComplement() {
        super(Model.class, "Negative Dezimalzahl &rarr; Komplement-Darstellung");
    }

    @Override
    protected String getTemplateName() {
        return "NegativeDecToBinaryComplement";
    }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // Create question model
        Long number = -randomDataGenerator.nextLong(1, 100);
        Long bitCount = Long.toBinaryString(-number).length() + 1 + randomDataGenerator.nextLong(0, 2);
        Complement complement = Complement.values()[randomDataGenerator.nextInt(0, Complement.values().length - 1)];

        // Create question text
        Context ctx = new Context();
        ctx.setVariable("number", number);
        ctx.setVariable("bitCount", bitCount);
        ctx.setVariable("complement", complement);

        var question = processQuestionTemplate(templateEngine, ctx);

        // Create model
        var model = Model.builder()
                .number(number)
                .bitCount(bitCount)
                .complement(complement)
                .build();

        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        return 100 * 3 * Complement.values().length;
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        Model model = (Model) modelObject;

        // Get question configuration
        Long number = model.getNumber();
        Long bitCount = model.getBitCount();
        Complement complement = model.getComplement();

        // Create integer conversion table on positive number
        Fragments.IntegerConversionTable integerConversionResult = Fragments.createIntegerConversionTable(Long.valueOf(-number), NumberBase.BINARY);

        String fullBitString = leftPadString(integerConversionResult.getResult(), '0', bitCount.intValue());
        String oneComplementString = fullBitString.replace('0', '!').replace('1', '0').replace('!', '1');

        Context ctx = new Context();

        ctx.setVariable("number", number);
        ctx.setVariable("bitCount", bitCount);
        ctx.setVariable("complement", complement);
        ctx.setVariable("integerConversionResult", integerConversionResult);
        ctx.setVariable("bitCount", bitCount);
        ctx.setVariable("binaryIntegerFullBit", fullBitString);
        ctx.setVariable("oneComplementResult", oneComplementString);

        if(complement == Complement.TWO) {
            Fragments.BinaryComputationResult binaryComputationResult = Fragments.createAdditionComputationTable(oneComplementString, "1", "+", NumberBase.BINARY);
            ctx.setVariable("twoComplementAdditionTable", binaryComputationResult);
            ctx.setVariable("twoComplementResult", binaryComputationResult.getResult().trim());
        }

        return processSolutionTemplate(templateEngine, ctx);
    }

}
