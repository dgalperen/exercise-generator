package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model;

public class LocaleConstants {
    /**
     * Separator for decimal numbers.
     */
    public static final String DECIMAL_SEPARATOR = ",";
}
