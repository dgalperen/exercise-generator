package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StringUtil {
    /**
     * Add characters to the beginning of a string until a target length is reached.
     * @param string string
     * @param ch character
     * @param targetLength target length
     * @return resulting string
     */
    public static String leftPadString(String string, char ch, int targetLength) {
        String replacement = String.format("%c", ch);
        return new String(new char[targetLength - string.length()]).replace("\0", replacement) + string;
    }

    /**
     * Add characters to the end of a string until a target length is reached.
     * @param string string
     * @param ch character
     * @param targetLength target length
     * @return resulting string
     */
    public static String rightPadString(String string, char ch, int targetLength) {
        if(targetLength - string.length() < 0) {
            return string;
        }
        String replacement = String.format("%c", ch);
        return string + new String(new char[targetLength - string.length()]).replace("\0", replacement);
    }

    /**
     * Converts a number to a binary string with a fixed number of digits.
     * Number is filled with zeros on the left.
     * @param value value
     * @param length target length of string
     * @return string
     */
    public static String decimalToBinaryStringFixedLength(int value, int length){
        if (length <= 0) {
            return "";
        }
        return String.format("%" + length + "s", Integer.toBinaryString(value)).replaceAll(" ", "0");
    }
}
