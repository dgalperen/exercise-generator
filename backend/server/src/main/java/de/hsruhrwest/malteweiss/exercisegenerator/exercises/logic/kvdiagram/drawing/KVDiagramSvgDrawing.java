package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.drawing;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model.CoverageTermTO;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model.KVMatrix;
import lombok.extern.log4j.Log4j2;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGeneratorContext;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * SVG drawing of KV diagram
 */
@Log4j2
public class KVDiagramSvgDrawing {

    /**
     * Regular font for text content
     */
    private Font regularFont;

    /**
     * Bold font for text content
     */
    private Font boldFont;

    /**
     * Metrics for drawing KV diagram
     */
    private static class DiagramMetrics {
        // Left axis legend
        public static final int LEFT_AXIS_LEGEND_WIDTH = 25;
        public static final int LEFT_AXIS_LEGEND_SPACE_RIGHT = 2;

        // Top axis legend
        public static final int TOP_AXIS_LEGEND_HEIGHT = 28;
        public static final int TOP_AXIS_LEGEND_SPACE_BOTTOM = 2;

        // Left label width and space to field
        public static final int LEFT_VARIABLE_LABELS_WIDTH = 35;
        public static final int LEFT_VARIABLE_LABELS_SPACE_RIGHT = 2;

        // Top label height and space to field
        public static final int TOP_VARIABLE_LABELS_HEIGHT = 35;
        public static final int TOP_VARIABLE_LABELS_SPACE_BOTTOM = 2;

        // Field width and height
        public static final int FIELD_WIDTH = 32;
        public static final int FIELD_HEIGHT = 32;
        public static final int FIELD_SPACING = 2;

        // Number of pixels that cover term rectangle may exceed field area
        public static final int COVER_TERM_PIXELS_BEYOND_FIELD_AREA = 2;

        // Corner radius of rectangle for cover term
        public static final int COVER_TERM_RECTANGLE_CORNER_RADIUS = 10;

        // The offset that a cover rectangle is shrunk by index
        // (thereby boundaries of cover rectangles do not overlap exactly)
        public static final double COVER_TERM_RECTANGLE_SHRINK_OFFSET_PER_TERM = 1;

        // Stroke of a cover term rectangle
        public static final float COVE_TERM_RECTANGLE_STROKE_WIDTH = 3;

        // Computed sizes
        public static final int FIELD_AREA_WIDTH = 4 * FIELD_WIDTH + 3 * FIELD_SPACING;
        public static final int FIELD_AREA_HEIGHT = 4 * FIELD_HEIGHT + 3 * FIELD_SPACING;
        public static final int FIELD_START_X = LEFT_AXIS_LEGEND_WIDTH + LEFT_AXIS_LEGEND_SPACE_RIGHT + LEFT_VARIABLE_LABELS_WIDTH + LEFT_VARIABLE_LABELS_SPACE_RIGHT;
        public static final int FIELD_START_Y = TOP_AXIS_LEGEND_HEIGHT + TOP_AXIS_LEGEND_SPACE_BOTTOM + TOP_VARIABLE_LABELS_HEIGHT + TOP_VARIABLE_LABELS_SPACE_BOTTOM;
        public static final int TOTAL_DIAGRAM_WIDTH = FIELD_START_X + FIELD_AREA_WIDTH + COVER_TERM_PIXELS_BEYOND_FIELD_AREA;
        public static final int TOTAL_DIAGRAM_HEIGHT = FIELD_START_Y + FIELD_AREA_HEIGHT + COVER_TERM_PIXELS_BEYOND_FIELD_AREA;
    }

    /**
     * Colors in KV diagram.
     */
    private static class DiagramColors {
        // Background of diagram
        public static final Color DIAGRAM_BACKGROUND_COLOR = Color.WHITE;

        // Background of field
        public static final Color FIELD_BACKGROUND_COLOR = new Color(225, 225, 225);

        // Color of coverage rectangle that is currently not in fokus
        public static final Color COVERAGE_COLOR_NO_FOCUS = new Color(128, 128, 128);

        // Revise this - find better colors
        public static final Color[] COVERAGE_COLORS = {
                new Color(38, 138,189),
                new Color(0, 128,0),
                new Color(148, 17,0),
                new Color(255, 147,0),
                new Color(218, 96,250),
                new Color(162, 149,60),
                new Color(38, 162,119),
                new Color(87, 78,162)
        };
    }

    /**
     * Fonts in KV diagram.
     */
    private static class DiagramFonts {
        // Default font
        public static final String DEFAULT_FONT_FILENAME = "fonts/Roboto-Regular.ttf";
        public static final float DEFAULT_FONT_SIZE = 20.0f;

        // Variable font
        public static final String BOLD_FONT_FILENAME = "fonts/Roboto-Bold.ttf";
        public static final float BOLD_FONT_SIZE = 20.0f;
    }

    /**
     * Flags for creating diagram
     */
    public enum DiagramCreationFlags {
        /** If set, no values output rendered in diagram. */
        NO_VALUES,
        /** If set, only the last cover term is colorized. */
        ONLY_COLORIZE_LAST_TERM;
    }

    /**
     * Constructs KV diagram SVG drawing object.
     */
    public KVDiagramSvgDrawing() {
        // Create fonts for rendering
        regularFont = loadFontFromStream(DiagramFonts.DEFAULT_FONT_FILENAME, DiagramFonts.DEFAULT_FONT_SIZE);
        boldFont = loadFontFromStream(DiagramFonts.BOLD_FONT_FILENAME, DiagramFonts.BOLD_FONT_SIZE);
    }

    /**
     * Loads TrueType font from resource.
     * @param resourceName resource
     * @param fontSize size of font
     * @return font object
     * @throws IllegalStateException if font cannot be loaded
     */
    private Font loadFontFromStream(String resourceName, float fontSize) {
        try(InputStream stream = KVDiagramSvgDrawing.class.getClassLoader().getResourceAsStream(resourceName)) {
            return Font.createFont(Font.TRUETYPE_FONT, stream).deriveFont(DiagramFonts.DEFAULT_FONT_SIZE);
        } catch (FontFormatException | IOException e) {
            log.error(e);
            throw new IllegalStateException(e);
        }
    }

    /**
     * Create SVG graphic for coverage terms
     * @param function function as KV matrix
     * @param coverTerms cover terms
     * @param flags flags for creation of diagram
     * @return SVG string
     */
    public String createDiagram(KVMatrix function, java.util.List<CoverageTermTO> coverTerms, java.util.List<DiagramCreationFlags> flags) {
        // Get a DOMImplementation.
        DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();

        // Create an instance of org.w3c.dom.Document.
        String svgNS = "http://www.w3.org/2000/svg";
        Document document = domImpl.createDocument(svgNS, "svg", null);

        // Ensure that fonts are embedded.
        SVGGeneratorContext ctx = SVGGeneratorContext.createDefault(document);
        ctx.setEmbeddedFontsOn(true);

        // Create an instance of the SVG Generator.
        SVGGraphics2D svgGraphics2D = new SVGGraphics2D(ctx, true);
        svgGraphics2D.setSVGCanvasSize(new Dimension(DiagramMetrics.TOTAL_DIAGRAM_WIDTH, DiagramMetrics.TOTAL_DIAGRAM_HEIGHT));
        svgGraphics2D.setBackground(DiagramColors.DIAGRAM_BACKGROUND_COLOR);

        // Set default font
        svgGraphics2D.setFont(regularFont);

        // Draw background
        svgGraphics2D.setPaint(DiagramColors.DIAGRAM_BACKGROUND_COLOR);
        svgGraphics2D.fillRect(0, 0, DiagramMetrics.TOTAL_DIAGRAM_WIDTH, DiagramMetrics.TOTAL_DIAGRAM_HEIGHT);

        // Draw field grid
        svgGraphics2D.setPaint(DiagramColors.FIELD_BACKGROUND_COLOR);
        svgGraphics2D.fillRect(DiagramMetrics.FIELD_START_X, DiagramMetrics.FIELD_START_Y, DiagramMetrics.FIELD_AREA_WIDTH, DiagramMetrics.FIELD_AREA_HEIGHT);

        svgGraphics2D.setPaint(DiagramColors.DIAGRAM_BACKGROUND_COLOR);
        for(int i = 0; i < 3; i++) {
            int yStart = DiagramMetrics.FIELD_START_Y + DiagramMetrics.FIELD_HEIGHT + i * (DiagramMetrics.FIELD_HEIGHT + DiagramMetrics.FIELD_SPACING);
            svgGraphics2D.fillRect(DiagramMetrics.FIELD_START_X, yStart, DiagramMetrics.FIELD_AREA_WIDTH, DiagramMetrics.FIELD_SPACING);
        }
        for(int i = 0; i < 3; i++) {
            int xStart = DiagramMetrics.FIELD_START_X + DiagramMetrics.FIELD_WIDTH + i * (DiagramMetrics.FIELD_WIDTH + DiagramMetrics.FIELD_SPACING);
            svgGraphics2D.fillRect(xStart, DiagramMetrics.FIELD_START_Y, DiagramMetrics.FIELD_SPACING, DiagramMetrics.FIELD_AREA_HEIGHT);
        }

        // Label "AB"
        svgGraphics2D.setFont(boldFont);
        {
            svgGraphics2D.setColor(Color.BLACK);
            drawCenteredText(svgGraphics2D, "AB",
                    new Rectangle(DiagramMetrics.FIELD_START_X, 0, DiagramMetrics.FIELD_AREA_WIDTH, DiagramMetrics.TOP_AXIS_LEGEND_HEIGHT));
        }

        // Label "CD" (rotated by -90 degree)
        {
            Rectangle rc = new Rectangle(0, DiagramMetrics.FIELD_START_Y, DiagramMetrics.LEFT_AXIS_LEGEND_WIDTH, DiagramMetrics.FIELD_AREA_HEIGHT);

            AffineTransform atOld = svgGraphics2D.getTransform();
            svgGraphics2D.setTransform(AffineTransform.getRotateInstance(Math.toRadians(-90), rc.getCenterX(), rc.getCenterY()));

            drawCenteredText(svgGraphics2D, "CD", rc);

            svgGraphics2D.setTransform(atOld);
        }

        // Left variable labels (00, 01, 11, 10)
        {
            String[] labels = {"00", "01", "11", "10"};
            svgGraphics2D.setFont(regularFont);
            for(int y = 0; y < 4; y++) {
                Rectangle fieldRect = new Rectangle(
                        DiagramMetrics.LEFT_AXIS_LEGEND_WIDTH + DiagramMetrics.LEFT_AXIS_LEGEND_SPACE_RIGHT,
                        DiagramMetrics.FIELD_START_Y + y * (DiagramMetrics.FIELD_HEIGHT + DiagramMetrics.FIELD_SPACING),
                        DiagramMetrics.LEFT_VARIABLE_LABELS_WIDTH,
                        DiagramMetrics.FIELD_HEIGHT);
                drawCenteredText(svgGraphics2D, labels[y], fieldRect);
            }
            for(int x = 0; x < 4; x++) {
                Rectangle fieldRect = new Rectangle(
                        DiagramMetrics.FIELD_START_X + x * (DiagramMetrics.FIELD_WIDTH + DiagramMetrics.FIELD_SPACING),
                        DiagramMetrics.TOP_AXIS_LEGEND_HEIGHT + DiagramMetrics.TOP_AXIS_LEGEND_SPACE_BOTTOM,
                        DiagramMetrics.FIELD_WIDTH,
                        DiagramMetrics.TOP_VARIABLE_LABELS_HEIGHT);
                drawCenteredText(svgGraphics2D, labels[x], fieldRect);
            }
        }

        // Draw values from truth table
        if(!flags.contains(DiagramCreationFlags.NO_VALUES)) {
            svgGraphics2D.setColor(Color.BLACK);
            svgGraphics2D.setFont(regularFont);
            for (int y = 0; y < 4; y++) {
                for (int x = 0; x < 4; x++) {
                    Rectangle fieldRect = new Rectangle(
                            DiagramMetrics.FIELD_START_X + x * (DiagramMetrics.FIELD_WIDTH + DiagramMetrics.FIELD_SPACING),
                            DiagramMetrics.FIELD_START_Y + y * (DiagramMetrics.FIELD_HEIGHT + DiagramMetrics.FIELD_SPACING),
                            DiagramMetrics.FIELD_WIDTH,
                            DiagramMetrics.FIELD_HEIGHT);
                    drawCenteredText(svgGraphics2D, function.get(x, y) ? "1" : "0", fieldRect);
                }
            }
        }

        // Clip coverage terms in case of overlaps beyond boundaries
        svgGraphics2D.setClip(DiagramMetrics.FIELD_START_X - DiagramMetrics.COVER_TERM_PIXELS_BEYOND_FIELD_AREA, DiagramMetrics.FIELD_START_Y - DiagramMetrics.COVER_TERM_PIXELS_BEYOND_FIELD_AREA, DiagramMetrics.FIELD_AREA_WIDTH + 2*DiagramMetrics.COVER_TERM_PIXELS_BEYOND_FIELD_AREA, DiagramMetrics.FIELD_AREA_HEIGHT + 2*DiagramMetrics.COVER_TERM_PIXELS_BEYOND_FIELD_AREA);

        // Draw all coverage term rectangles
        for(CoverageTermTO term : coverTerms) {
            int index = coverTerms.indexOf(term);

            // Determine color. If onlyColorizeLastTerm is true, only the last
            // rectangle is drawn in color.
            Color color;
            if(flags.contains(DiagramCreationFlags.ONLY_COLORIZE_LAST_TERM) && index != coverTerms.size() - 1) {
                color = DiagramColors.COVERAGE_COLOR_NO_FOCUS;
            } else {
                color = DiagramColors.COVERAGE_COLORS[index % DiagramColors.COVERAGE_COLORS.length];
            }

            // Draw rectangle.
            drawCoverTermRectangle(svgGraphics2D, term, color, index * DiagramMetrics.COVER_TERM_RECTANGLE_SHRINK_OFFSET_PER_TERM);
        }

        // Generate SVG file and return it as string
        try (StringWriter sw = new StringWriter()) {
            svgGraphics2D.stream(sw, true);
            return sw.toString();
        } catch (IOException e) {
            log.error(e);
            throw new IllegalStateException(e);
        }
    }

    /**
     * Draws rectangle(s) for cover term.
     * @param graphics graphics context
     * @param term cover term
     * @param color color of rectangle(s)
     * @param rectangleShrinkOffset offset that rectangles are shrunk by
     */
    private void drawCoverTermRectangle(Graphics2D graphics, CoverageTermTO term, Color color, double rectangleShrinkOffset) {
        // Create list of drawing rectangles for this coverage.
        // Special cases are considered where rectangles overlap diagram boundaries.
        List<Rectangle> rectangles = new ArrayList<>();
        if(term.getMinX() == 3 && term.getMinY() == 3 && term.getMaxX() == 4 && term.getMaxY() == 4) {
            // Special case 1: Horizontal and vertical overlap beyond bottom right corner with 2x2 rectangle
            //
            //   O..O
            //   ....
            //   ....
            //   O..O
            //
            rectangles.add(new Rectangle(3, 3, 2, 2));
            rectangles.add(new Rectangle(-1, -1, 2, 2));
            rectangles.add(new Rectangle(3, -1, 2, 2));
            rectangles.add(new Rectangle(-1, 3, 2, 2));
        } else if(term.getMaxX() == 4) {
            // Special case 2: Horizontal overlap beyond right border
            //
            //   ....
            //   O..O
            //   ....
            //   ....
            //
            rectangles.add(new Rectangle(term.getMinX(), term.getMinY(), term.getMaxX() - term.getMinX() + 1, term.getMaxY() - term.getMinY() + 1));
            rectangles.add(new Rectangle(-1, term.getMinY(), 2, term.getMaxY() - term.getMinY() + 1));
        } else if(term.getMaxY() == 4) {
            // Special case 3: Vertical overlap beyond bottom border
            //
            //   .O..
            //   ....
            //   ....
            //   .O..
            //
            rectangles.add(new Rectangle(term.getMinX(), term.getMinY(), term.getMaxX() - term.getMinX() + 1, term.getMaxY() - term.getMinY() + 1));
            rectangles.add(new Rectangle(term.getMinX(), -1, term.getMaxX() - term.getMinX() + 1, 2));
        } else {
            // Regular case: Rectangle within diagram boundaries
            rectangles.add(new Rectangle(term.getMinX(), term.getMinY(), term.getMaxX() - term.getMinX() + 1, term.getMaxY() - term.getMinY() + 1));
        }

        // Draw rectangles
        graphics.setColor(color);
        graphics.setStroke(new BasicStroke(DiagramMetrics.COVE_TERM_RECTANGLE_STROKE_WIDTH));
        for(Rectangle rectangle : rectangles) {
            // Compute SVG coordinates
            double x = DiagramMetrics.FIELD_START_X + rectangle.getX() * (DiagramMetrics.FIELD_WIDTH + DiagramMetrics.FIELD_SPACING);
            double y = DiagramMetrics.FIELD_START_Y + rectangle.getY() * (DiagramMetrics.FIELD_HEIGHT + DiagramMetrics.FIELD_SPACING);
            double width = rectangle.getWidth() * DiagramMetrics.FIELD_WIDTH + (rectangle.getWidth() - 1) * DiagramMetrics.FIELD_SPACING;
            double height = rectangle.getHeight() * DiagramMetrics.FIELD_WIDTH + (rectangle.getHeight() - 1) * DiagramMetrics.FIELD_SPACING;

            // Shrink rectangle by parameter
            x += rectangleShrinkOffset;
            y += rectangleShrinkOffset;
            width -= 2 * rectangleShrinkOffset;
            height -= 2 * rectangleShrinkOffset;

            // Draw rectangle
            graphics.drawRoundRect((int) x, (int) y, (int) width, (int) height, DiagramMetrics.COVER_TERM_RECTANGLE_CORNER_RADIUS, DiagramMetrics.COVER_TERM_RECTANGLE_CORNER_RADIUS);
        }
    }

    /**
     * Draw text that is horizontally and vertically centered in a box.
     * @param graphics graphics context
     * @param text text
     * @param rectangle rectangle in which text is centered
     */
    public static void drawCenteredText(Graphics2D graphics, String text, Rectangle rectangle) {
        var currentFont = graphics.getFont();
        var metrics = graphics.getFontMetrics(currentFont);

        int width = metrics.stringWidth(text);

        double x = rectangle.getX() + rectangle.getWidth() / 2 - width / 2;
        double y = rectangle.getY() + rectangle.getHeight() / 2 - metrics.getHeight() / 2 + metrics.getAscent();

        graphics.drawString(text, (float) x, (float) y);
    }
}
