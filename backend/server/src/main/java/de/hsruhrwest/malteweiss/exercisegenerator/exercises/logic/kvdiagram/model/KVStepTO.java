package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class KVStepTO {
    private String description;
    private String diagram;
}
