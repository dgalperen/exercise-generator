package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.ConversionUtil;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

import static de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.LocaleConstants.DECIMAL_SEPARATOR;

@Log4j2
@Exercise(id = "BinOctHexIntegerToDecimal")
public class BinOctHexIntegerToDecimal extends AbstractExerciseGenerator {

    /**
     * If this attribute is true, numbers with decimals are created.
     */
    @Setter
    private boolean enableDecimals;

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private String number;
        private NumberBase base;
    }

    /**
     * Constructs object with enabled decimals.
     */
    public BinOctHexIntegerToDecimal() {
        this(true, "Binäre/oktale/hexadezimale Kommazahl &rarr; dezimale Darstellung");
    }

    /**
     * Constructs object and sets flags.
     * @param enableDecimals determines whether decimals should be created.
     * @param name name of exercise
     */
    public BinOctHexIntegerToDecimal(boolean enableDecimals, String name) {
        super(Model.class, name);
        this.enableDecimals = enableDecimals;
    }

    @Override
    protected String getTemplateName() {
        return "BinOctHexToDecimal";
    }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // Create non-decimal random base and number
        NumberBase base;
        do {
            base = NumberBase.values()[randomDataGenerator.nextInt(0, NumberBase.values().length - 1)];
        } while(base == NumberBase.DECIMAL);

        // Create integer part
        long number;
        switch(base) {
            case BINARY:
                number = randomDataGenerator.nextLong(0b10000000L, 0b11111111L);
                break;
            case OCTAL:
                number = randomDataGenerator.nextLong(010000L, 077777L);
                break;
            case HEXADECIMAL:
                number = randomDataGenerator.nextLong(0x1000L, 0xFFFFL);
                break;
            default:
                throw new IllegalStateException("Unknown base: " + base);
        }
        String numberAsString = ConversionUtil.numberToStringInBase(number, base);

        // Create decimal part
        if(enableDecimals) {
            String decimalNumberPart;
            switch(base) {
                case BINARY:
                    decimalNumberPart = String.format("%4s",
                        ConversionUtil.numberToStringInBase(randomDataGenerator.nextLong(1, 0b1111L), base)
                    ).replace(" ", "0");
                    break;
                case OCTAL:
                    decimalNumberPart = String.format("%2s",
                        ConversionUtil.numberToStringInBase(randomDataGenerator.nextLong(1, 077L), base)
                    ).replace(" ", "0");
                    break;
                case HEXADECIMAL:
                    decimalNumberPart = String.format("%2s",
                        ConversionUtil.numberToStringInBase(randomDataGenerator.nextLong(1, 0xFFL), base)
                    ).replace(" ", "0");
                    break;
                default:
                    throw new IllegalStateException("Unknown base: " + base);
            }
            numberAsString += DECIMAL_SEPARATOR + decimalNumberPart;
        }

        // Create question text
        Context ctx = new Context();
        ctx.setVariable("number", numberAsString);
        ctx.setVariable("base", base.getNumericBase());
        var question = processQuestionTemplate(templateEngine, ctx);

        // Create model
        Model model = Model.builder()
                .number(numberAsString)
                .base(base)
                .build();

        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        long variants =
                (0b11111111L - 0b10000000L + 1) +
                (077777L - 010000L + 1) +
                (0xFFFFL - 0x1000L + 1);
        if(enableDecimals) {
            variants *= (0b1111L + 077L + 0xFFL);
        }
        return variants;
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        Model model = (Model) modelObject;

        // Get question configuration
        String originalNumber = model.getNumber();
        String numberWithoutDecimalPoint = originalNumber.replace(DECIMAL_SEPARATOR, "");
        int numericBase = model.getBase().getNumericBase();

        int numDecimals = 0;
        if(originalNumber.indexOf(DECIMAL_SEPARATOR) != -1) {
            numDecimals = originalNumber.length() - originalNumber.indexOf(DECIMAL_SEPARATOR) - 1;
        }

        // Extract number
        Number pureIntegerNumber;
        switch(model.getBase()) {
            case BINARY:
                pureIntegerNumber = Long.parseLong(numberWithoutDecimalPoint, 2);
                break;
            case OCTAL:
                pureIntegerNumber = Long.parseLong(numberWithoutDecimalPoint, 8);
                break;
            case HEXADECIMAL:
                pureIntegerNumber = Long.parseLong(numberWithoutDecimalPoint, 16);
                break;
            default:
                throw new IllegalStateException("Unknown base: " + model.getBase());
        }

        String numberAsString = ConversionUtil.numberToStringInBase(pureIntegerNumber.longValue(), model.getBase());

        List<String> rows = new ArrayList<>();

        // B^x notation
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < numberAsString.length(); i++) {
                int digit = ConversionUtil.digitToDecimal(numberAsString.charAt(i));
                builder.append(String.format("%s<td><b>%d</b> &middot; %d<sup>%d</sup></td>",
                        i > 0 ? "<td>+</td>" : "",
                        digit,
                        numericBase,
                        numberAsString.length() - i - 1 - numDecimals));
            }
            rows.add("<tr><td></td>" + builder.toString() + "</tr>");
        }

        // integer factor notation
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < numberAsString.length(); i++) {
                int digit = ConversionUtil.digitToDecimal(numberAsString.charAt(i));
                builder.append(
                        String.format("%s<td>%s<b>%d</b> &middot; %s%s</sup></td>",
                            i > 0 ? "<td>+</td>" : "",
                            digit == 0 ? "<span class='text-muted'>" : "",
                            digit,
                            ConversionUtil.toReadableNumberString(BigDecimal.valueOf(numericBase).pow(numberAsString.length() - i - 1 - numDecimals, MathContext.DECIMAL128)),
                            digit == 0 ? "</span>" : "")
                );
            }
            rows.add("<tr><td>=</td>" + builder.toString() + "</tr>");
        }

        // integer notation
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < numberAsString.length(); i++) {
                int digit = ConversionUtil.digitToDecimal(numberAsString.charAt(i));
                if(digit == 0) {
                    builder.append("<td></td><td></td>");
                } else {
                    builder.append(
                            String.format("%s<td>%s</sup></td>",
                                    i > 0 ? "<td>+</td>" : "",
                                    ConversionUtil.toReadableNumberString(
                                        BigDecimal
                                            .valueOf(digit)
                                            .multiply(BigDecimal.valueOf(numericBase).pow(numberAsString.length() - i - 1 - numDecimals, MathContext.DECIMAL128))))
                    );
                }
            }
            rows.add(String.format("<tr><td>=</td>%s</tr>", builder));
        }

        BigDecimal result =
            BigDecimal
                .valueOf(pureIntegerNumber.doubleValue())
                .multiply(BigDecimal.valueOf(numericBase).pow(-numDecimals, MathContext.DECIMAL128));
        String resultString = ConversionUtil.toReadableNumberString(result);
        rows.add(String.format("<tr><td>=</td><td colspan='%d'>%s</td></tr>", 2*numberAsString.length(), resultString));

        Context ctx = new Context();
        ctx.setVariable("rows", rows);
        ctx.setVariable("originalNumber", originalNumber);
        ctx.setVariable("originalBase", numericBase);
        ctx.setVariable("N", numberWithoutDecimalPoint.length() - numDecimals);
        ctx.setVariable("M", numDecimals);
        ctx.setVariable("B", numericBase);
        ctx.setVariable("result", resultString);

        return processSolutionTemplate(templateEngine, ctx);
    }

}
