package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.booltabletonf;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Exercise(id = "BoolTableToKNF")
public class BoolTableToKNF extends AbstractBoolTableToNFBase {

    public BoolTableToKNF() {
        super("Wahrheitstabelle in Konjunktive Normalform (KNF) &uuml;berf&uuml;hren");
    }

    @Override
    protected boolean isRelevantRow(boolean truthTableValue) {
        return !truthTableValue;
    }

    @Override
    protected String getParameterPrefixForLetter(boolean parameterValue) {
        return parameterValue == true ? "&#xAC;" : "";
    }

    @Override
    protected String getSolutionInnerOperatorHtml() {
        return "&or;";
    }

    @Override
    protected String getSolutionOuterOperatorHtml() {
        return "&and;";
    }

    @Override
    protected String getTemplateTaskText() {
        return "Stellen Sie die gegebene Funktion in der konjunktiven Normalform (KNF) dar.";
    }

    @Override
    protected String getTemplateAnswerText() {
        return "Die konjunktive Normalform (KNF) sieht für die oben gezeigte Wahrheitstabelle wie folgt aus:";
    }

}
