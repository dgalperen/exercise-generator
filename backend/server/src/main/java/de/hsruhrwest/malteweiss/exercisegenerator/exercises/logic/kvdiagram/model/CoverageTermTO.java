package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model;

import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Coverage term in configuration file.
 */
@Data
public class CoverageTermTO {
    private String rectangle;
    private String expression;

    private int minX;
    private int minY;
    private int maxX;
    private int maxY;

    private static final Pattern RECTANGLE_PATTERN = Pattern.compile("\\(([0-9]+),([0-9]+)\\)-\\(([0-9]+),([0-9]+)\\)");

    /**
     * Setter for rectangle string that also sets coordinates (minX,minY)-(maxX,maxY).
     * @param rectangleString rectangle as string in format (#,#)-(#,#)
     */
    public void setRectangle(String rectangleString) {
        this.rectangle = rectangleString;

        Matcher matcher = RECTANGLE_PATTERN.matcher(rectangleString);
        if(!matcher.matches() || 4 != matcher.groupCount()) {
            throw new IllegalArgumentException("Incorrect rectangle definition: " + rectangleString);
        }

        minX = Integer.parseInt(matcher.group(1).toString());
        minY = Integer.parseInt(matcher.group(2).toString());
        maxX = Integer.parseInt(matcher.group(3).toString());
        maxY = Integer.parseInt(matcher.group(4).toString());
    }

    /**
     * @return size of rectangle in number of covered fields
     */
    public int getSize() {
        return (maxX - minX + 1) * (maxY - minY + 1);
    }

    @Override
    public String toString() {
        return String.format("(%d,%d)-(%d,%d)", minX, minY, maxX, maxY);
    }
}
