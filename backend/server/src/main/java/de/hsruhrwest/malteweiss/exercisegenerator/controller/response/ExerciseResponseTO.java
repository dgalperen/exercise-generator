package de.hsruhrwest.malteweiss.exercisegenerator.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExerciseResponseTO {
    /**
     * Name of exercise.
     */
    private String name;

    /**
     * Exam that exercise was taken from.
     * Number of variants of exercise
     */
    private long numberOfVariants;

    /**
     * Exam that exercise was taken from.
     */
    private String exam;

    /**
     * Description of answer.
     */
    private String htmlQuestion;

    /**
     * Description of solution.
     */
    private String htmlSolution;
}
