package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.fragments.Fragments;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.ConversionUtil;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

@Log4j2
@Exercise(id = "BinaryAddition")
public class BinaryAddition extends AbstractExerciseGenerator {
   
    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private String firstNumber;
        private String secondNumber;
    }    

    @Override
    protected String getTemplateName() {
        return "BinaryAddition";
    }

    public BinaryAddition() { super(Model.class, "Addition im Binärsystem"); }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // Create vars for question model
        Integer number1 = randomDataGenerator.nextInt(32, 128);
        Integer number2 = randomDataGenerator.nextInt(32, 128);

        // Create question text
        Context ctx = new Context();
        ctx.setVariable("number1", number1);
        ctx.setVariable("number2", number2);

        String question = processQuestionTemplate(templateEngine, ctx);

        Model model = Model.builder().firstNumber(Long.toString(number1)).secondNumber(Long.toString(number2)).build();

        return new ExerciseTO(question, model);
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        Model model = (Model) modelObject;

        Integer firstNumber = Integer.parseInt(model.getFirstNumber());
        String firstNumberBinary = Integer.toBinaryString(firstNumber);
        Integer secondNumber = Integer.parseInt(model.getSecondNumber());
        String secondNumberBinary = Integer.toBinaryString(secondNumber);

        Integer result = firstNumber + secondNumber;
        String resultBinary = Integer.toBinaryString(result);

        // Pad numbers with leading zeros
        String firstNumberPadded = String.format("%8s", firstNumberBinary).replace(' ', '0');
        String secondNumberPadded = String.format("%8s", secondNumberBinary).replace(' ', '0');
        String resultPadded = String.format("%8s", resultBinary).replace(' ', '0');

        // Construct overflow String
        StringBuilder overflow = new StringBuilder("       0");

        // Calculate the next overflow (from least significant bit to most significant bit)
        for(int i = 7; i > 0; i--) {
            if((firstNumberPadded.charAt(i) + overflow.charAt(i) + secondNumberPadded.charAt(i)) >= ('1' + '1' + '0'))
                overflow.setCharAt(i-1,'1');
            else
                overflow.setCharAt(i-1, '0');
        }

        // integer notation
        String resultConversionToDecimal;
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < resultBinary.length(); i++) {
                int digit = ConversionUtil.digitToDecimal(resultBinary.charAt(i));
                builder.append(String.format("%s<td><b>%d</b> &middot; %d<sup>%d</sup></td>",
                        i > 0 ? "<td>+</td>" : "",
                        digit,
                        2,
                        resultBinary.length() - i - 1));
            }
            resultConversionToDecimal = "<tr><td></td>" + builder.toString() + "<td>=</td><td>" + result + "</td></tr>";
        }

        // Create solution text
        Context ctx = new Context();
        ctx.setVariable("firstNumber", firstNumber);
        ctx.setVariable("secondNumber", secondNumber);
        ctx.setVariable("conversionResultFirstOperand", Fragments.createIntegerConversionTable(firstNumber, NumberBase.BINARY));
        ctx.setVariable("conversionResultSecondOperand", Fragments.createIntegerConversionTable(secondNumber, NumberBase.BINARY));
        ctx.setVariable("firstNumberBinary", firstNumberPadded);
        ctx.setVariable("secondNumberBinary", secondNumberPadded);
        ctx.setVariable("result", result);
        ctx.setVariable("resultBinary", resultPadded);
        ctx.setVariable("resultConversionToDecimal", resultConversionToDecimal);
        ctx.setVariable("overflow", overflow.toString());

        Fragments.BinaryComputationResult additionResult = Fragments.createAdditionComputationTable(firstNumberPadded, secondNumberPadded, "+", NumberBase.BINARY);
        ctx.setVariable("twoComplementAdditionTable", additionResult);
        ctx.setVariable("twoComplementResult", additionResult.getResult().trim());

        return processSolutionTemplate(templateEngine, ctx);
    }

    @Override
    public long getNumberOfVariants() {
        return (128-32+1) * (128-32+1);
    }

}
