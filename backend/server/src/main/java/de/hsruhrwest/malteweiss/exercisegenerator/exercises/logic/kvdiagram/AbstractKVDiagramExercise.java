package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram;

import com.esotericsoftware.yamlbeans.YamlReader;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.drawing.KVDiagramSvgDrawing;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model.CoverageMintermConfigurationTO;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model.CoverageTermTO;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model.KVMatrix;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model.KVStepTO;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Log4j2
public abstract class AbstractKVDiagramExercise extends AbstractExerciseGenerator {
    private static final String KV_DIAGRAM_COVERAGE_MINTERM_CONFIGURATIONS = "templates/KVDiagram/kv-diagram-coverage-minterm-configurations.yaml";

    /**
     * Potential coverage terms. These are read from configuration.
     */
    private List<CoverageTermTO> potentialCoverageTerms;

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private Map<Integer, Integer> function;
    }

    /**
     * Constructs object
     */
    public AbstractKVDiagramExercise(String name) {
        super(Model.class, name);

        // Read configuration
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(getClass().getClassLoader().getResource(KV_DIAGRAM_COVERAGE_MINTERM_CONFIGURATIONS).openStream(), StandardCharsets.UTF_8))) {
            // Read configuration
            YamlReader reader = new YamlReader(br);
            CoverageMintermConfigurationTO configGroupList = reader.read(CoverageMintermConfigurationTO.class);
            potentialCoverageTerms = configGroupList.getTerms();
            reader.close();
        } catch (IOException e) {
            log.error("Cannot read configuration for KV diagram coverages.");
            throw new IllegalStateException(e);
        }
    }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // Create model
        int dimension = 4;
        int maxValue = (int) Math.pow(2, dimension) - 1;

        Map<Integer, Integer> function = new HashMap<>();
        for (int i = 0; i <= maxValue; i++) {
            function.put(i, randomDataGenerator.nextInt(0, 1));
        }

        // Create question text
        Context ctx = new Context();

        // Truth table
        List<String> truthTableRows = new ArrayList<>();

        truthTableRows.add("<tr>"
                + IntStream.range(0, dimension).mapToObj(i -> String.format("<td>%c</td>", 'A' + i)).collect(Collectors.joining())
                + "<td>" + getFunctionDefinitionString(dimension) + "</td>"
                + "</tr>");

        for (int value = 0; value <= maxValue; value++) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < dimension; i++) {
                boolean bit = (value & (1 << (dimension - i - 1))) != 0;
                builder.append(String.format("<td>%s</td>", bit ? "1" : "0"));
            }
            builder.append(String.format("<td>%s</td>", function.get(value) != 0 ? "1" : "0"));
            truthTableRows.add("<tr>" + builder.toString() + "</tr>");
        }

        ctx.setVariable("truthTableRows", truthTableRows);
        ctx.setVariable("localization", getLocalizedWords());

        Model model = Model.builder()
                .function(function)
                .build();

        var question = processQuestionTemplate(templateEngine, ctx);
        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        return (long) Math.pow(2, 16);
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        // Extract function and initialize remaining function that has to be covered
        KVMatrix function = getFunctionFromModel((Model) modelObject);
        KVMatrix remainingFunction = function.copy();

        // Cover remaining function until everything is covered
        var resultCoverTerms = new ArrayList<CoverageTermTO>();
        while (remainingFunction.getNumberOfSpecificFields(getCoverValue()) > 0) {
            // Find best term that covers remaining fields. We are searching for the largest term that
            // covers new fields but may also cover existing fields (overlaps reduce term size).
            CoverageTermTO bestTerm = null;
            int highestCoverage = 0;
            for (CoverageTermTO term : potentialCoverageTerms) {
                // Term must cover original function
                if (function.canTermCoverFunction(term, getCoverValue())) {
                    // Term must cover as much new fields as possible
                    // (note: remainingFunction is used here)
                    int remainingCoverage = remainingFunction.countCoverage(term, getCoverValue());
                    if (remainingCoverage > 0) {
                        // If remaining coverage is higher than existing best term,
                        // or remaining coverage is the same but term is larger,
                        //   then this is the newest best term.
                        if (remainingCoverage > highestCoverage ||
                            (remainingCoverage == highestCoverage && term.getSize() > bestTerm.getSize())) {
                            highestCoverage = remainingCoverage;
                            bestTerm = term;
                        }
                    }
                }
            }

            // Subtract coverage
            remainingFunction = remainingFunction.getSubtractedFunction(bestTerm, !getCoverValue());

            // Add term to list of result
            resultCoverTerms.add(bestTerm);
        }

        KVDiagramSvgDrawing svgDiagram = new KVDiagramSvgDrawing();

        // Create all KV steps
        var kvStepList = new ArrayList<KVStepTO>();
        for (int i = 0; i < resultCoverTerms.size(); i++) {
            CoverageTermTO term = resultCoverTerms.get(i);

            String expression = getExpressionOutput(term.getExpression());
            String description;
            if (i == 0) {
                String stepAdjective = ((resultCoverTerms.size() == 1) ? "einzige" : "erste");
                description = "Das " + stepAdjective + " Rechteck, das wir überdecken, ist das folgende, das durch den Ausdruck <b>" + expression + "</b> repräsentiert wird:";
            } else if (i == resultCoverTerms.size() - 1) {
                description = "Zum Schluss überdecken wir das folgende Rechteck, zu dem der Ausdruck <b>" + expression + "</b> gehört:";
            } else {
                description = "Als nächstes überdecken wir das folgende Rechteck mit dem Ausdruck <b>" + expression + "</b>:";
            }

            kvStepList.add(KVStepTO.builder()
                    .description(String.format("Schritt %d: %s", i + 1, description))
                    .diagram(svgDiagram.createDiagram(function, resultCoverTerms.subList(0, i + 1), Arrays.asList(KVDiagramSvgDrawing.DiagramCreationFlags.ONLY_COLORIZE_LAST_TERM)))
                    .build());
        }

        // Create final formula rows
        List<String> kvFormulaRows = new ArrayList<>();
        boolean first = true;
        for (var coverTerm : resultCoverTerms) {
            StringBuilder builder = new StringBuilder();
            if (first) {
                builder.append(String.format("<td>%s =</td><td></td>", getFunctionDefinitionString(4)));
                first = false;
            } else {
                builder.append(String.format("<td></td><td>%s</td>", getSolutionOuterOperatorHtml()));
            }
            builder.append("<td>(</td>");
            builder.append("<td>" + getExpressionOutput(coverTerm.getExpression()) + "</td>");
            builder.append("<td>)</td>");
            kvFormulaRows.add(builder.toString());
        }

        // Assign solution fields
        Context ctx = new Context();
        ctx.setVariable("emptyKVDiagram", svgDiagram.createDiagram(function, List.of(), Arrays.asList(KVDiagramSvgDrawing.DiagramCreationFlags.NO_VALUES)));
        ctx.setVariable("KVDiagramNoCoverage", svgDiagram.createDiagram(function, List.of(), List.of()));
        ctx.setVariable("KVStepList", kvStepList);
        ctx.setVariable("KVDiagramFinal", svgDiagram.createDiagram(function, resultCoverTerms, List.of()));
        ctx.setVariable("KVFormulaRows", kvFormulaRows);
        ctx.setVariable("normFormFormulaRows", getFormulaRows(((Model) modelObject).getFunction(), 4));
        ctx.setVariable("noRectangles", resultCoverTerms.size() == 0);
        ctx.setVariable("localization", getLocalizedWords());
        return processSolutionTemplate(templateEngine, ctx);
    }

    /**
     * Creates string for function definition: f(A,B,C,...)
     *
     * @param dimension dimension of function
     * @return definition string
     */
    private String getFunctionDefinitionString(int dimension) {
        return String.format("f(%s)", IntStream.range(0, dimension).mapToObj(j -> String.format("%c", 'A' + j)).collect(Collectors.joining(", ")));
    }

    @Override
    protected String getTemplateName() {
        return "KVDiagram";
    }

    /**
     * Returns function from question model as 4x4 array.
     *
     * @param model model
     * @return 4x4 boolean array, first coordinate is x (column), second is y (row)
     */
    private KVMatrix getFunctionFromModel(Model model) {
        KVMatrix matrix = new KVMatrix();

        matrix.set(0, 0, model.getFunction().get(0b0000) != 0);
        matrix.set(1, 0, model.getFunction().get(0b0100) != 0);
        matrix.set(2, 0, model.getFunction().get(0b1100) != 0);
        matrix.set(3, 0, model.getFunction().get(0b1000) != 0);

        matrix.set(0, 1, model.getFunction().get(0b0001) != 0);
        matrix.set(1, 1, model.getFunction().get(0b0101) != 0);
        matrix.set(2, 1, model.getFunction().get(0b1101) != 0);
        matrix.set(3, 1, model.getFunction().get(0b1001) != 0);

        matrix.set(0, 2, model.getFunction().get(0b0011) != 0);
        matrix.set(1, 2, model.getFunction().get(0b0111) != 0);
        matrix.set(2, 2, model.getFunction().get(0b1111) != 0);
        matrix.set(3, 2, model.getFunction().get(0b1011) != 0);

        matrix.set(0, 3, model.getFunction().get(0b0010) != 0);
        matrix.set(1, 3, model.getFunction().get(0b0110) != 0);
        matrix.set(2, 3, model.getFunction().get(0b1110) != 0);
        matrix.set(3, 3, model.getFunction().get(0b1010) != 0);

        return matrix;
    }

    /**
     * Generates all rows for a formula table.
     *
     * @param function  function
     * @param dimension dimension of function
     * @return formula rows as strings
     */
    private List<String> getFormulaRows(Map<Integer, Integer> function, int dimension) {
        List<String> formulaRows = new ArrayList<>();

        int maxValue = (int) Math.pow(2, dimension) - 1;

        // Special case: All values are 0
        if (function.values().stream().allMatch(v -> v.equals(0))) {
            formulaRows.add("<tr>"
                    + "<td>" + getFunctionDefinitionString(dimension) + "</td>"
                    + "<td>=</td>"
                    + "<td>0</td>"
                    + "</tr>");
        }
        // Special case: All values are 1
        else if (function.values().stream().allMatch(v -> v.equals(1))) {
            formulaRows.add("<tr>"
                    + "<td>" + getFunctionDefinitionString(dimension) + "</td>"
                    + "<td>=</td>"
                    + "<td>1</td>"
                    + "</tr>");
        }
        // Regular case
        else {
            boolean first = true;
            for (int value = 0; value <= maxValue; value++) {
                if (isRelevantRow(function.get(value) != 0)) {
                    StringBuilder builder = new StringBuilder();
                    if (first) {
                        builder.append(String.format("<td>%s =</td><td></td>", getFunctionDefinitionString(dimension)));
                        first = false;
                    } else {
                        builder.append(String.format("<td></td><td>%s</td>", getSolutionOuterOperatorHtml()));
                    }
                    builder.append("<td>(</td>");
                    for (int i = 0; i < dimension; i++) {
                        boolean bit = (value & (1 << (dimension - i - 1))) != 0;
                        builder.append(String.format("<td>%s%c</td>", getParameterPrefixForLetter(bit), 'A' + i));
                        if (i < dimension - 1) {
                            builder.append(String.format("<td>%s</td>", getSolutionInnerOperatorHtml()));
                        }
                    }
                    builder.append("<td>)</td>");
                    formulaRows.add(builder.toString());
                }
            }
        }
        return formulaRows;
    }

    /**
     * Denotes the value that is covered by rectangle.
     * @return true for 1, false for 0
     */
    protected abstract boolean getCoverValue();

    /**
     * @param truthTableValue function value of a row in the truth table
     * @return whether row in function f(A,B,C,...) is relevant for final formula
     */
    protected abstract boolean isRelevantRow(boolean truthTableValue);

    /**
     * @param parameterValue value that is set to a specific parameter
     * @return the prefix for a parameter A, B, C, ... depending on the value that is assigned to this parameter
     */
    protected abstract String getParameterPrefixForLetter(boolean parameterValue);

    /**
     * @return template task text
     */
    protected abstract String getSolutionInnerOperatorHtml();

    /**
     * @return template task text
     */
    protected abstract String getSolutionOuterOperatorHtml();

    /**
     * @param configMinTermExpression expression from min-term configuration file
     * @return expression for given term
     */
    protected abstract String getExpressionOutput(String configMinTermExpression);

    /**
     * @return localization of task-dependent words
     */
    protected abstract Map<String, String> getLocalizedWords();
}