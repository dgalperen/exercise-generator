package de.hsruhrwest.malteweiss.exercisegenerator.controller.response;

import java.util.List;

public class ExerciseGeneratorListResponseTO {
    /**
     * List of exercise generators.
     */
    private List<ExerciseGeneratorResponseTO> items;

    public ExerciseGeneratorListResponseTO(List<ExerciseGeneratorResponseTO> items) {
        this.items = items;
    }

    public List<ExerciseGeneratorResponseTO> getItems() {
        return this.items;
    }

    public void setItems(List<ExerciseGeneratorResponseTO> items) {
        this.items = items;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof ExerciseGeneratorListResponseTO))
            return false;
        final ExerciseGeneratorListResponseTO other = (ExerciseGeneratorListResponseTO) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$items = this.getItems();
        final Object other$items = other.getItems();
        if (this$items == null ? other$items != null : !this$items.equals(other$items)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ExerciseGeneratorListResponseTO;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $items = this.getItems();
        result = result * PRIME + ($items == null ? 43 : $items.hashCode());
        return result;
    }

    public String toString() {
        return "ExerciseGeneratorListResponseTO(items=" + this.getItems() + ")";
    }
}
