package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.booltabletonf;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Log4j2
public abstract class AbstractBoolTableToNFBase extends AbstractExerciseGenerator {
    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private int dimension;
        private Map<Integer, Integer> function;
    }

    public AbstractBoolTableToNFBase(String name) {
        super(Model.class, name);
    }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // Create model
        int dimension = 3;
        int maxValue = (int) Math.pow(2, dimension) - 1;

        Map<Integer, Integer> function = new HashMap<>();
        for(int i = 0; i <= maxValue; i++) {
            function.put(i, randomDataGenerator.nextInt(0, 1));
        }

        // Create question text
        Context ctx = new Context();

        // Truth table
        List<String> truthTableRows = new ArrayList<>();

        truthTableRows.add("<tr>"
                + IntStream.range(0, dimension).mapToObj(i -> String.format("<td>%c</td>", 'A' + i)).collect(Collectors.joining())
                + "<td>" + getFunctionDefinitionString(dimension) + "</td>"
                + "</tr>");

        for(int value = 0; value <= maxValue; value++) {
            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < dimension; i++) {
                boolean bit = (value & (1 << (dimension - i - 1))) != 0;
                builder.append(String.format("<td>%s</td>", bit ? "w" : "f"));
            }
            builder.append(String.format("<td>%s</td>", function.get(value) != 0 ? "w" : "f"));
            truthTableRows.add("<tr>" + builder.toString() + "</tr>");
        }

        ctx.setVariable("truthTableRows", truthTableRows);
        ctx.setVariable("taskText", getTemplateTaskText());
        var question = processQuestionTemplate(templateEngine, ctx);

        Model model = Model.builder()
            .dimension(dimension)
            .function(function)
            .build();

        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        return (long) Math.pow(2, 8);
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        Model model = (Model) modelObject;

        int dimension = model.getDimension();
        Map<Integer, Integer> function = model.getFunction();

        // Additional hint
        String additionalHint = null;
        if(function.values().stream().allMatch(v -> v.equals(0))) {
            // Special case: All values are 0
            additionalHint = "Die Funktion ist unerfüllbar/widerspruchsvoll.";
        }
        else if(function.values().stream().allMatch(v -> v.equals(1))) {
            // Special case: All values are 1
            additionalHint = "Die Funktion ist allgemeingültig.";
        }

        Context ctx = new Context();
        ctx.setVariable("formulaRows", getFormulaRows(function, dimension));
        ctx.setVariable("answerText", getTemplateAnswerText());
        ctx.setVariable("additionalHint", additionalHint);

        return processSolutionTemplate(templateEngine, ctx);
    }

    /**
     * Generates all rows for a formula table.
     * @param function function
     * @param dimension dimension of function
     * @return formular rows as strings
     */
    private List<String> getFormulaRows(Map<Integer, Integer> function, int dimension) {
        List<String> formulaRows = new ArrayList<>();

        int maxValue = (int) Math.pow(2, dimension) - 1;

        // Special case: All values are 0
        if(function.values().stream().allMatch(v -> v.equals(0))) {
            formulaRows.add("<tr>"
                    + "<td>" + getFunctionDefinitionString(dimension) + "</td>"
                    + "<td>=</td>"
                    + "<td>0</td>"
                    + "</tr>");
        }
        // Special case: All values are 1
        else if(function.values().stream().allMatch(v -> v.equals(1))) {
            formulaRows.add("<tr>"
                    + "<td>" + getFunctionDefinitionString(dimension) + "</td>"
                    + "<td>=</td>"
                    + "<td>1</td>"
                    + "</tr>");
        }
        // Regular case
        else {
            boolean first = true;
            for (int value = 0; value <= maxValue; value++) {
                if (isRelevantRow(function.get(value) != 0)) {
                    StringBuilder builder = new StringBuilder();
                    if (first) {
                        builder.append(String.format("<td>%s =</td><td></td>", getFunctionDefinitionString(dimension)));
                        first = false;
                    } else {
                        builder.append(String.format("<td></td><td>%s</td>", getSolutionOuterOperatorHtml()));
                    }
                    builder.append("<td>(</td>");
                    for (int i = 0; i < dimension; i++) {
                        boolean bit = (value & (1 << (dimension - i - 1))) != 0;
                        builder.append(String.format("<td>%s%c</td>", getParameterPrefixForLetter(bit), 'A' + i));
                        if (i < dimension - 1) {
                            builder.append(String.format("<td>%s</td>", getSolutionInnerOperatorHtml()));
                        }
                    }
                    builder.append("<td>)</td>");
                    formulaRows.add(builder.toString());
                }
            }
        }
        return formulaRows;
    }

    /**
     * Creates string for function definition: f(A,B,C,...)
     * @param dimension dimension of function
     * @return definition string
     */
    private String getFunctionDefinitionString(int dimension) {
        return String.format("f(%s)", IntStream.range(0, dimension).mapToObj(j -> String.format("%c", 'A' + j)).collect(Collectors.joining(", ")));
    }

    @Override
    protected String getTemplateName() {
        return "BoolTableToNF";
    }

    /**
     * @param truthTableValue function value of a row in the truth table
     * @return whether row in function f(A,B,C,...) is relevant for final formula
     */
    protected abstract boolean isRelevantRow(boolean truthTableValue);

    /**
     * @param parameterValue value that is set to a specific parameter
     * @return the prefix for a parameter A, B, C, ... depending on the value that is assigned to this parameter
     */
    protected abstract String getParameterPrefixForLetter(boolean parameterValue);

    /**
     * @return template task text
     */
    protected abstract String getSolutionInnerOperatorHtml();

    /**
     * @return template task text
     */
    protected abstract String getSolutionOuterOperatorHtml();

    /**
     * @return template task text
     */
    protected abstract String getTemplateTaskText();

    /**
     * @return template answer text
     */
    protected abstract String getTemplateAnswerText();
}
