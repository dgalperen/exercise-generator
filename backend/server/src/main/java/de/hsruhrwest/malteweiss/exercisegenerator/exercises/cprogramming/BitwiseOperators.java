package de.hsruhrwest.malteweiss.exercisegenerator.exercises.cprogramming;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.fragments.Fragments;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.StringUtil;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;


@Log4j2
@Exercise(id = "BitwiseOperators")
public class BitwiseOperators extends AbstractExerciseGenerator {

    /**
     * Maximum number of bits used for operands.
     */
    private final int BIT_COUNT = 6;

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private int leftOperand;
        private int rightOperand;
        private String operator;
    }

    public BitwiseOperators () {
        super(Model.class, "Bitweise Operatoren", "CProgramming/BitwiseOperators");
    }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // create random operands
        int leftOperand = randomDataGenerator.nextInt(1, (int) (Math.pow(2, BIT_COUNT) - 1));
        int rightOperand = randomDataGenerator.nextInt(1, (int) (Math.pow(2, BIT_COUNT) - 1));

        // randomly determine which bitwise operator will be used
        String operator =
            switch (randomDataGenerator.nextInt(0,2)) {
                case 0 -> "|";
                case 1 -> "&";
                default -> "^";
            };

        // create question text
        Context ctx = new Context();
        ctx.setVariable("leftOperand", leftOperand);
        ctx.setVariable("leftOperandBinary", Integer.toBinaryString(leftOperand));
        ctx.setVariable("rightOperand", rightOperand);
        ctx.setVariable("rightOperandBinary", Integer.toBinaryString(rightOperand));
        ctx.setVariable("operator", operator);

        var question = processQuestionTemplate(templateEngine, ctx);

        // create model
        Model model = Model.builder()
                .leftOperand(leftOperand)
                .rightOperand(rightOperand)
                .operator(operator)
                .build();

        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        // left operand * 3 operators * right operand
        return (int) (Math.pow(2, BIT_COUNT) - 1) * 3 * (int) (Math.pow(2, BIT_COUNT) - 1);
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        Model model = (Model) modelObject;

        // create a solution based on randomized operator
        Integer result = switch(model.getOperator()) {
            case "|" -> (model.getLeftOperand() | model.getRightOperand());
            case "&" -> (model.getLeftOperand() & model.getRightOperand());
            case "^" -> (model.getLeftOperand() ^ model.getRightOperand());
            default -> throw new IllegalStateException("Unknown operator: " + model.getOperator());
        };

        // create binary strings
        String leftOperandBinary = StringUtil.decimalToBinaryStringFixedLength(model.getLeftOperand(), BIT_COUNT);
        String rightOperandBinary = StringUtil.decimalToBinaryStringFixedLength(model.getRightOperand(), BIT_COUNT);
        String resultBinary = StringUtil.decimalToBinaryStringFixedLength(result, BIT_COUNT);

        // Create solution text
        Context ctx = new Context();
        ctx.setVariable("result", result);
        ctx.setVariable("leftOperand", model.getLeftOperand());
        ctx.setVariable("rightOperand", model.getRightOperand());
        ctx.setVariable("leftOperandBinary", leftOperandBinary);
        ctx.setVariable("rightOperandBinary", rightOperandBinary);
        ctx.setVariable("operator", model.getOperator());
        ctx.setVariable("numBits", BIT_COUNT);
        ctx.setVariable("resultBinary", resultBinary);
        ctx.setVariable("computationTable", Fragments.createBinaryComputationTable(leftOperandBinary, rightOperandBinary, "", model.getOperator(), resultBinary, NumberBase.BINARY));

        return processSolutionTemplate(templateEngine, ctx);
    }
}
