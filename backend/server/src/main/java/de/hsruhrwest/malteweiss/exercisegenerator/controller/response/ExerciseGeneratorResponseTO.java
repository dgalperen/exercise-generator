package de.hsruhrwest.malteweiss.exercisegenerator.controller.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExerciseGeneratorResponseTO {
    /**
     * ID of exercise.
     */
    private String id;

    /**
     * Name of exercise.
     */
    private String name;
}
