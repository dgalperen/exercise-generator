package de.hsruhrwest.malteweiss.exercisegenerator.controller;

import de.hsruhrwest.malteweiss.exercisegenerator.controller.response.ExerciseGroupListTO;
import de.hsruhrwest.malteweiss.exercisegenerator.controller.response.ExerciseResponseTO;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import de.hsruhrwest.malteweiss.exercisegenerator.services.ExerciseService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.io.IOException;
import java.util.NoSuchElementException;

@Log4j2
@RestController
public class ExerciseController {

    @Autowired
    private ExerciseService exerciseService;

    @Autowired
    private SpringTemplateEngine springTemplateEngine;

    /**
     * Returns all exercise groups.
     * @return list of exercise groups containing exercise descriptions
     */
    @GetMapping(value = "/api/exercise-groups")
    public ResponseEntity<ExerciseGroupListTO> getAllExerciseGroups() {
        log.info("Getting exercise groups.");

        try {
            return ResponseEntity.ok(new ExerciseGroupListTO(exerciseService.getAvailableExerciseGroups()));
        } catch (IOException | NoSuchElementException e) {
            log.error(e);
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Returns an exercise.
     * @param id ID of the exercise
     * @return exercise with signed model
     */
    @GetMapping(value = "/api/exercise/{id}")
    public ResponseEntity<ExerciseResponseTO> getExercise(@PathVariable String id) {
        try {
            log.info("Getting exercise " + id + ".");

            AbstractExerciseGenerator exerciseGenerator = exerciseService.getExerciseGenerator(id);

            exerciseService.logExerciseAccess(exerciseGenerator);   // log this access

            ExerciseTO exerciseTO = exerciseGenerator.createExercise(springTemplateEngine);
            ExerciseResponseTO exerciseResponseTO = ExerciseResponseTO.builder()
                    .name(exerciseGenerator.getName())
                    .exam(exerciseGenerator.getExam())
                    .numberOfVariants(exerciseGenerator.getNumberOfVariants())
                    .htmlQuestion(exerciseTO.getHtmlDescription())
                    .htmlSolution(exerciseGenerator.createSolution(springTemplateEngine, exerciseTO.getModel()))
                    .build();
            return ResponseEntity.ok(exerciseResponseTO);
        } catch(NoSuchElementException ex) {
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {
            log.error(ex);
            return ResponseEntity.internalServerError().build();
        }
    }
}
