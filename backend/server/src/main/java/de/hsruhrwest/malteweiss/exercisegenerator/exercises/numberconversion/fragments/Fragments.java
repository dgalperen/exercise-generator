package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.fragments;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.ConversionUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.LocaleConstants.DECIMAL_SEPARATOR;

@Log4j2
public class Fragments {

    @Data
    @AllArgsConstructor
    public static class IntegerConversionTable {
        /** Conversion rows for template output. */
        List<Map<String, Object>> conversionRows;

        /** Result of conversion. */
        String result;

        /** Base of result. */
        Integer resultBase;
    }

    @Data
    @AllArgsConstructor
    @Builder
    public static class BinaryComputationResult {
        /** First summand */
        String summand1;

        /** Second summand */
        String summand2;

        /** Operator */
        String operator;

        /** Carry */
        String carryOver;

        /** Result */
        String result;

        /** Base. */
        NumberBase base;

        public String toString() {
            return String.format("  %s\n +%s\n  %s\n-------------\n= %s\n", summand1, summand2, carryOver, result);
        }
    }

    /**
     * Generates conversion table to demonstrate conversion between decimal number to other system.
     * @param decimalNumber integer number
     * @param targetBase base
     * @return conversion table
     */
    public static IntegerConversionTable createIntegerConversionTable(long decimalNumber, NumberBase targetBase) {
        if(decimalNumber == 0) {
            return new IntegerConversionTable(new ArrayList<>(), "0", targetBase.getNumericBase());
        }

        long currentNumber = decimalNumber;
        StringBuilder reversedResult = new StringBuilder();

        List<Map<String, Object>> integerConversionRows = new ArrayList<>();
        while (currentNumber > 0) {
            long residual = currentNumber % targetBase.getNumericBase();

            reversedResult.append(String.format("%c", ConversionUtil.decimalToDigit((char) residual)));

            Map<String, Object> row = new HashMap<>();
            row.put("currentNumber", currentNumber);
            row.put("base", targetBase.getNumericBase());
            row.put("integerResult", currentNumber / targetBase.getNumericBase());
            row.put("residual", residual);
            row.put("digit", String.format("%c", ConversionUtil.decimalToDigit((int) residual)));
            integerConversionRows.add(row);

            currentNumber /= targetBase.getNumericBase();
        }
        return new IntegerConversionTable(integerConversionRows, reversedResult.reverse().toString(), targetBase.getNumericBase());
    }

    /**
     * Compute addition table with two summands, carry over line and result line.
     * @param summand1 first summand
     * @param summand2 second summand
     * @param operator operator
     * @param base numeric base
     * @return result of addition
     */
    public static BinaryComputationResult createBinaryComputationTable(String summand1, String summand2, String carryOver, String operator, String result, NumberBase base) {
        StringBuilder reversedSummand1 = new StringBuilder(summand1).reverse();
        StringBuilder reversedSummand2 = new StringBuilder(summand2).reverse();
        StringBuilder reversedCarryOver = new StringBuilder(carryOver).reverse();
        StringBuilder reversedResult = new StringBuilder(result).reverse();

        int maxChars = Math.max(Math.max(Math.max(reversedSummand1.length(), reversedSummand2.length()), reversedCarryOver.length()), reversedResult.length());

        // Left pad all strings
        while(reversedSummand1.length() < maxChars) reversedSummand1.append(" ");
        while(reversedSummand2.length() < maxChars) reversedSummand2.append(" ");
        while(reversedCarryOver.length() < maxChars) reversedCarryOver.append(" ");
        while(reversedResult.length() < maxChars) reversedResult.append(" ");

        // Replace leading zeros with spaces
        return BinaryComputationResult.builder()
            .summand1(replaceLeadingZerosWithSpaces(reversedSummand1.reverse().toString()))
            .summand2(replaceLeadingZerosWithSpaces(reversedSummand2.reverse().toString()))
            .operator(operator)
            .carryOver(reversedCarryOver.reverse().toString().replace('0', ' '))
            .result(replaceLeadingZerosWithSpaces(reversedResult.reverse().toString()))
            .base(base)
            .build();
    }

    /**
     * Compute addition table with two summands, carry over line and result line.
     * @param summand1 first summand
     * @param summand2 second summand
     * @param operator operator
     * @param base numeric base
     * @return result of addition
     */
    public static BinaryComputationResult createAdditionComputationTable(String summand1, String summand2, String operator, NumberBase base) {
        StringBuilder reversedSummand1 = new StringBuilder(summand1).reverse();
        StringBuilder reversedSummand2 = new StringBuilder(summand2).reverse();
        StringBuilder reversedCarryOver = new StringBuilder("0");
        StringBuilder reversedResult = new StringBuilder();

        int numericBase = base.getNumericBase();

        int maxChars = Math.max(reversedSummand1.length(), reversedSummand2.length()) + 1;

        // Compute sum
        for(int i = 0; i < maxChars; i++) {
            int digit1 = (i >= reversedSummand1.length()) ? 0 : ConversionUtil.digitToDecimal(reversedSummand1.charAt(i));
            int digit2 = (i >= reversedSummand2.length()) ? 0 : ConversionUtil.digitToDecimal(reversedSummand2.charAt(i));
            int carryOver = ConversionUtil.digitToDecimal(reversedCarryOver.charAt(i));

            int resultDigit = digit1 + digit2 + carryOver;
            reversedResult.append(String.format("%c", ConversionUtil.decimalToDigit(resultDigit % numericBase)));
            if(i < maxChars-1) {
                reversedCarryOver.append(resultDigit >= numericBase ? "1" : "0");
            }
        }

        return createBinaryComputationTable(summand1, summand2, reversedCarryOver.reverse().toString(), operator, reversedResult.reverse().toString(), base);
    }

    @Data
    public static class AnyNumberSystemToDecimalFormularResult {
        /** Numeric base of source number */
        private int numericBase;

        /** List of summands */
        private List<Summand> summands;

        /** Conversion result */
        private String result;

        @Data
        public static class Summand {
            /** Factor of a summand */
            private long factor;

            /** Exponent of a summand */
            private long exponent;
        }
    }

    /**
     * Converts number in string format in base base to struture that represents formular in
     * form of: 1 * 2^0 + 0 * 2^1 + 1 * 2^1 + ... = result
     * @param numberAsString number as string
     * @param base base of original number
     * @param showZeroFactors if true zero factors are shown
     * @return result for fragment
     */
    public static AnyNumberSystemToDecimalFormularResult anyNumberSystemToDecimalFormula(String numberAsString, NumberBase base, boolean showZeroFactors) {
        final int numDecimals =
            (numberAsString.indexOf(DECIMAL_SEPARATOR) != -1) ?
                numberAsString.length() - numberAsString.indexOf(DECIMAL_SEPARATOR) - 1 :
                0;

        var summands = new ArrayList<AnyNumberSystemToDecimalFormularResult.Summand>();

        BigDecimal resultValue = BigDecimal.ZERO;

        String numberNoDecimals = numberAsString.replace(DECIMAL_SEPARATOR, "");

        for(int i = 0; i < numberNoDecimals.length(); i++) {
            int exponent = numberNoDecimals.length() - i - 1 - numDecimals;
            int factor = ConversionUtil.digitToDecimal(numberNoDecimals.charAt(i));

            if(factor == 0 && !showZeroFactors) {
                continue;
            }

            var summand = new AnyNumberSystemToDecimalFormularResult.Summand();
            summand.setExponent(exponent);
            summand.setFactor(factor);
            summands.add(summand);

            resultValue = resultValue.add(BigDecimal.valueOf(factor).multiply(BigDecimal.valueOf(base.getNumericBase()).pow(exponent, MathContext.DECIMAL128)));
        }

        var result = new AnyNumberSystemToDecimalFormularResult();
        result.setNumericBase(base.getNumericBase());
        result.setSummands(summands);
        result.setResult(ConversionUtil.toReadableNumberString(resultValue));

        return result;
    }

    /**
     * Replaces leading zeros in a string with spaces.
     * @param numberString number string
     * @return string with replaced leading zeros
     */
    private static String replaceLeadingZerosWithSpaces(String numberString) {
        StringBuilder stringBuilder = new StringBuilder(numberString);
        for(int i = 0; i < stringBuilder.length(); i++) {
            if(stringBuilder.charAt(i) == ' ') {
                // no op
            } else if(stringBuilder.charAt(i) == '0') {
                stringBuilder.setCharAt(i, ' ');
            } else {
                break;
            }
        }
        return stringBuilder.toString();
    }

    @Data
    @AllArgsConstructor
    @Builder
    public static class DecimalToBinOctHexResult {
        /** target base */
        int targetBase;

        /** integer part */
        String integerPart;

        /** decimal part */
        String decimalPart;

        /** result of integer conversion */
        IntegerConversionTable integerConversionResult;

        /** rows of integer conversion table */
        List<Map<String, Object>> decimalConversionRows = new ArrayList<>();

        /** decimal result as string */
        String decimalResult;

        /** conversion result with maximum number of digits and without recursion */
        String maxDigitResult;

        /** true iff conversion result has a repetend */
        boolean hasRepetend;
    }

    /**
     * Full number. May contain comma.
     * @param fullNumber full number
     * @param targetBase target base
     * @param maxSolutionDigits maximum number of digits that decimal part may produce
     * @return conversion result that can be used in fragment
     */
    public static DecimalToBinOctHexResult createDecimalToBinOctHexConversion(String fullNumber, NumberBase targetBase, int maxSolutionDigits) {
        int numericBase = targetBase.getNumericBase();

        String integerPart, decimalPart;
        if(fullNumber.contains(DECIMAL_SEPARATOR)) {
            String[] items = fullNumber.split(DECIMAL_SEPARATOR);
            integerPart = items[0];
            decimalPart = items[1];
        } else {
            integerPart = fullNumber;
            decimalPart = null;
        }

        // --- INTEGER PART ---

        // Create integer conversion table
        Fragments.IntegerConversionTable integerConversionResult = Fragments.createIntegerConversionTable(Long.valueOf(integerPart), targetBase);

        // --- DECIMAL PART ---

        List<Map<String, Object>> decimalConversionRows = new ArrayList<>();
        String decimalResult = null;
        String maxDigitResult = null;
        boolean hasRepetend = false;
        if(decimalPart == null) {
            // Create result string with maximum digits from integer result
            StringBuilder digitResult = new StringBuilder(integerConversionResult.result);
            if(maxSolutionDigits > 0) {
                digitResult.append(",");
                for (int i = 0; i < maxSolutionDigits; i++) {
                    digitResult.append("0");
                }
            }
            maxDigitResult = digitResult.toString();
        } else {
            BigDecimal currentNumber = ConversionUtil.germanStringToBigDecimal("0," + decimalPart);

            List<BigDecimal> previousNumbers = new ArrayList<>();
            StringBuilder allDigits = new StringBuilder();
            Integer recursionStartIndex = null;
            for(int i = 0; i < maxSolutionDigits && currentNumber.compareTo(BigDecimal.ZERO) != 0; i++) {
                BigDecimal product = currentNumber.multiply(BigDecimal.valueOf(numericBase));
                BigDecimal nextNumber = product.subtract(BigDecimal.valueOf(product.longValue()));

                int digit = product.intValue();
                String digitString = String.format("%c", ConversionUtil.decimalToDigit(digit));

                boolean endByZero = nextNumber.compareTo(BigDecimal.ZERO) == 0;

                // Check if we check this number already (period)
                var recursionCompare = nextNumber;
                boolean endByRecursion =
                        previousNumbers.stream().anyMatch(n -> n.compareTo(recursionCompare) == 0) ||
                                nextNumber.compareTo(currentNumber) == 0;

                Map<String, Object> row = new HashMap<>();
                row.put("currentNumber", ConversionUtil.toReadableNumberString(currentNumber));
                row.put("nextNumber", ConversionUtil.toReadableNumberString(nextNumber));
                row.put("base", numericBase);
                row.put("decimalResult", ConversionUtil.toReadableNumberString(product, 1));
                row.put("overflow", digit);
                row.put("digit", digitString);
                row.put("beginOfRecursion", false);
                row.put("endByRecursionHint", endByRecursion);
                row.put("endByZeroHint", endByZero && !endByRecursion);
                row.put("lastDigitHint", i == maxSolutionDigits-1 && !endByZero && !endByRecursion);
                decimalConversionRows.add(row);

                allDigits.append(digitString);

                if(endByRecursion) {
                    for(int charIndex = previousNumbers.size()-1; charIndex >= 0; charIndex--) {
                        if(previousNumbers.get(charIndex).equals(recursionCompare)) {
                            recursionStartIndex = charIndex;
                            break;
                        }
                    }
                    if(recursionStartIndex != null) {
                        decimalConversionRows.get(recursionStartIndex).put("beginOfRecursion", true);
                    } else {
                        recursionStartIndex = decimalConversionRows.size() - 1;
                    }
                    break;
                }

                previousNumbers.add(currentNumber);
                currentNumber = nextNumber;
            }

            hasRepetend = (recursionStartIndex != null);

            if(!hasRepetend) {
                decimalResult = allDigits.toString();
            } else {
                decimalResult = String.format("%s<span class='infiniteNumberExpansion'>%s</span>",
                        allDigits.substring(0, recursionStartIndex), allDigits.substring(recursionStartIndex));
            }

            // Create version of decimal result that contains at least maxSolutionDigits digits
            String spreadOutDecimals;
            if(hasRepetend) {
                // Recursion. Repeat recursion until max solution digits is reached
                StringBuilder spreadOutResult = new StringBuilder(allDigits.substring(0, recursionStartIndex));
                while(spreadOutResult.length() < maxSolutionDigits) {
                    spreadOutResult.append(allDigits.substring(recursionStartIndex));
                }
                // Cut of digits after maximum length
                if(spreadOutResult.length() > maxSolutionDigits) {
                    spreadOutResult.delete(maxSolutionDigits, spreadOutResult.length());
                }
                spreadOutDecimals = spreadOutResult.toString();
            } else {
                // No recursion. Add zeros until max solution digits is reached
                StringBuilder spreadOutResult = new StringBuilder(allDigits.toString());
                while(spreadOutResult.length() < maxSolutionDigits) {
                    spreadOutResult.append("0");
                }
                spreadOutDecimals = spreadOutResult.toString();
            }
            maxDigitResult = integerConversionResult.result + DECIMAL_SEPARATOR + spreadOutDecimals;
        }

        return DecimalToBinOctHexResult.builder()
                .targetBase(numericBase)
                .integerPart(integerPart)
                .integerConversionResult(integerConversionResult)
                .decimalPart(decimalPart)
                .decimalConversionRows(decimalConversionRows)
                .decimalResult(decimalResult)
                .maxDigitResult(maxDigitResult)
                .hasRepetend(hasRepetend)
                .build();
    }
}
