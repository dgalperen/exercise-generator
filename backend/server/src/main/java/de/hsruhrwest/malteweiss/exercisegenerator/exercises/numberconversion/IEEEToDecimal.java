package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.AbstractExerciseGenerator;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.fragments.Fragments;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.NumberBase;
import de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.util.StringUtil;
import de.hsruhrwest.malteweiss.exercisegenerator.model.ExerciseTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import static de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion.model.LocaleConstants.DECIMAL_SEPARATOR;

@Log4j2
@Exercise(id = "IEEEToDecimal")
public class IEEEToDecimal extends AbstractExerciseGenerator {

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Model {
        private String number;
    }

    /**
     * Constructs object
     */
    public IEEEToDecimal() { super(Model.class, "Gleitpunktzahl im IEEE 754/854-Format &rarr; Dezimalzahl"); }

    @Override
    public ExerciseTO createExercise(SpringTemplateEngine templateEngine) {
        var randomDataGenerator = new RandomDataGenerator();

        // Create 32-bit binary IEEE number
        StringBuilder numberAsString = new StringBuilder();

        // - Random sign
        numberAsString.append(randomDataGenerator.nextInt(0, 1));

        // - Compute appropriate biased exponent
        int biasedExponent = randomDataGenerator.nextInt(-8, 8) + 127;
        numberAsString.append(StringUtil.leftPadString(Integer.toBinaryString(biasedExponent), '0', 8));

        // - Compute mantissa
        for (int i = 0; i < 23; i++) {
            if(i < 6) {
                numberAsString.append(randomDataGenerator.nextInt(0, 1));
            } else {
                numberAsString.append("0");
            }
        }

        // Create question text
        Context ctx = new Context();
        ctx.setVariable("number", numberAsString);
        var question = processQuestionTemplate(templateEngine, ctx);

        // Create model
        var model = Model.builder()
                .number(numberAsString.toString())
                .build();
        return new ExerciseTO(question, model);
    }

    @Override
    public long getNumberOfVariants() {
        return 2 * 17 * (long) Math.pow(2,6);
    }

    @Override
    public String createSolution(SpringTemplateEngine templateEngine, Object modelObject) {
        Model model = (Model) modelObject;

        // Get question configuration
        String originalNumber = model.getNumber();

        // Extract components from number
        String signBit = originalNumber.substring(0,1); // Vorzeichen
        String biasedExponentString = originalNumber.substring(1, 9);
        String originalFractionString = originalNumber.substring(9, 32); // Fraction

        // Build Context for template
        Context ctx = new Context();
        ctx.setVariable("originalNumber", originalNumber);

        // Compute actual exponent
        int biasedExponent = Integer.parseInt(originalNumber.substring(1, 9),2); // Exponent
        int actualExponent = biasedExponent - 127;

        // Compute result
        String binaryResult = removeExponentFromNumber("1," + originalFractionString, actualExponent);

        ctx.setVariable("signBit", signBit);
        ctx.setVariable("resultSign", signBit.equals("1") ? "-" : "");

        ctx.setVariable("biasedExponentString", biasedExponentString);
        ctx.setVariable("biasedExponentToDecTable", Fragments.anyNumberSystemToDecimalFormula(biasedExponentString, NumberBase.BINARY, true));
        ctx.setVariable("actualExponent", actualExponent);

        ctx.setVariable("originalFractionString", originalFractionString);
        ctx.setVariable("binaryResult", binaryResult);
        ctx.setVariable("binaryResultToDecTable", Fragments.anyNumberSystemToDecimalFormula(binaryResult, NumberBase.BINARY, false));

        return processSolutionTemplate(templateEngine, ctx);
    }

    /**
     * Converts binary number with given exponent factor (* 2^x) to number that does not
     * require exponent factor. Ending zero or decimal separator are removed.
     * Example:
     *   1,00022 * 2^2 -> 100,022
     *   100,022 * 2^-2 -> 1,00022
     * @param stringNumber binary number as string
     * @param exponent exponent that should be removed
     * @return binary number as string without exponent
     */
    protected static String removeExponentFromNumber(String stringNumber, int exponent) {
        StringBuilder newStringNumber = new StringBuilder(stringNumber);
        // Positive exponent
        if(exponent > 0) {
            for(; exponent > 0; exponent--) {
                int pos = newStringNumber.indexOf(DECIMAL_SEPARATOR);
                newStringNumber.delete(pos, pos + DECIMAL_SEPARATOR.length());
                newStringNumber.insert(pos+1, DECIMAL_SEPARATOR);
                if(newStringNumber.toString().endsWith(DECIMAL_SEPARATOR)) {
                    newStringNumber.append("0");
                }
            }
        }
        // Negative exponent
        else if(exponent < 0) {
            for(; exponent < 0; exponent++) {
                int pos = newStringNumber.indexOf(DECIMAL_SEPARATOR);
                newStringNumber.delete(pos, pos + DECIMAL_SEPARATOR.length());
                newStringNumber.insert(pos-1, DECIMAL_SEPARATOR);
                if(newStringNumber.toString().startsWith(DECIMAL_SEPARATOR)) {
                    newStringNumber.insert(0, "0");
                }
            }
        }

        // Shorten string if it ends with "0" or ","
        if(newStringNumber.toString().contains(DECIMAL_SEPARATOR)) {
            while(newStringNumber.toString().endsWith("0")) {
                newStringNumber.delete(newStringNumber.length()-1, newStringNumber.length());
            }
            if(newStringNumber.toString().endsWith(DECIMAL_SEPARATOR)) {
                newStringNumber.delete(newStringNumber.length()-1, newStringNumber.length());
            }
        }

        return newStringNumber.toString();
    }
}