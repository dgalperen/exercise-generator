package de.hsruhrwest.malteweiss.exercisegenerator.exercises.numberconversion;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Exercise(id = "IntegerToBinOctHex")
public class IntegerToBinOctHex extends DecimalToBinOctHex {

    /**
     * Constructs object.
     */
    public IntegerToBinOctHex() {
        super(false, "Ganzzahl &rarr; binäre/oktale/hexadezimale Darstellung");
    }

}
