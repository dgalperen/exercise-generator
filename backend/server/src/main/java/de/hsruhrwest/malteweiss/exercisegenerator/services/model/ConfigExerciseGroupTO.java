package de.hsruhrwest.malteweiss.exercisegenerator.services.model;

import lombok.Data;

import java.util.List;

/** Definition exercise group */
@Data
public class ConfigExerciseGroupTO {
    /** Name of group */
    private String name;

    /** Exam that exercise was taken from (optional) */
    private String exam;

    /** List of classes */
    private List<ExerciseConfigTO> exercises;
}
