package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram.model;

/**
 * Class denoting the Boolean matrix of a Karnaugh-Veitch (KV) diagram.
 */
public class KVMatrix {
    /**
     * Two-dimensional array containing values.
     */
    private boolean[][] values = new boolean[4][4];

    /**
     * Returns value from KV matrix.
     * @param x x-coordinate
     * @param y y-coordinate
     * @return value
     */
    public boolean get(int x, int y) {
        return values[x][y];
    }

    /**
     * Sets value in KV matrix.
     * @param x x-coordinate
     * @param y y-coordinate
     * @param value value to set
     */
    public void set(int x, int y, boolean value) {
        values[x][y] = value;
    }

    /**
     * Creates copy of this matrix.
     * @return copied matrix
     */
    public KVMatrix copy() {
        KVMatrix copy = new KVMatrix();
        for(int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                copy.set(x, y, this.get(x, y));
            }
        }
        return copy;
    }

    /**
     * Return number of fields in KV matrix with given value.
     * @param value value
     * @return number of fields with given value
     */
    public int getNumberOfSpecificFields(boolean value) {
        int count = 0;
        for(int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                if(get(x,y) == value) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Checks whether a term can cover a function given as KV diagram.
     * @param term term
     * @param value value that has to be covered
     * @return true iff term can cover the function
     */
    public boolean canTermCoverFunction(CoverageTermTO term, boolean value) {
        for(int y = term.getMinY(); y <= term.getMaxY(); y++) {
            for(int x = term.getMinX(); x <= term.getMaxX(); x++) {
                if(get(x % 4, y % 4) != value) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Counts the number of covered fields by term.
     * @param term term
     * @param value value that has to be covered
     * @return number of covered fields
     */
    public int countCoverage(CoverageTermTO term, boolean value) {
        int coverage = 0;
        for(int y = term.getMinY(); y <= term.getMaxY(); y++) {
            for(int x = term.getMinX(); x <= term.getMaxX(); x++) {
                if(get(x % 4, y % 4) == value) {
                    coverage++;
                }
            }
        }
        return coverage;
    }

    /**
     * Returns function after subtracting coverage term.
     * @param term term to subtract
     * @param nullValue value that is set for subtracted fields
     * @return subtracted function
     */
    public KVMatrix getSubtractedFunction(CoverageTermTO term, boolean nullValue) {
        KVMatrix result = new KVMatrix();
        for(int y = 0; y <= 4; y++) {
            for (int x = 0; x <= 4; x++) {
                if(x >= term.getMinX() && y >= term.getMinY() && x <= term.getMaxX() && y <= term.getMaxY()) {
                    result.set(x % 4, y % 4, nullValue);
                } else if(x < 4 && y < 4) {
                    result.set(x % 4, y % 4, get(x % 4, y % 4));
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(int y = 0; y < 4; y++) {
            builder.append(" ");
            for(int x = 0; x < 4; x++) {
                builder.append((get(x, y) ? 1 : 0) + " ");
                if(x < 3) {
                    builder.append("| ");
                }
            }
            builder.append("\n");
            if(y < 3) {
                builder.append("---+---+---+---\n");
            }
        }
        return builder.toString();
    }
}
