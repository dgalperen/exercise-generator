package de.hsruhrwest.malteweiss.exercisegenerator.services.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DescribedExerciseTO {
    /** ID of exercise */
    private String id;

    /** Name of exercise */
    private String name;

    /** Exam which exercise was taken from */
    private String exam;

    /** Number of variants */
    private long numVariants;

    /** Number of views */
    private long numViews;
}
