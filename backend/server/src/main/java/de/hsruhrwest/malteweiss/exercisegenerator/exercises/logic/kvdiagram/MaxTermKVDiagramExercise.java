package de.hsruhrwest.malteweiss.exercisegenerator.exercises.logic.kvdiagram;

import de.hsruhrwest.malteweiss.exercisegenerator.exercises.Exercise;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Exercise(id = "KVDiagramMaxTerm")
public class MaxTermKVDiagramExercise extends AbstractKVDiagramExercise {
    /**
     * Default constructor setting name of exercise.
     */
    public MaxTermKVDiagramExercise() {
        super("Aussagenlogische Formel mit der Maxterm-Methode vereinfachen");
    }

    @Override
    protected boolean getCoverValue() {
        return false;    // 0
    }

    @Override
    protected boolean isRelevantRow(boolean truthTableValue) {
        return !truthTableValue;
    }

    @Override
    protected String getParameterPrefixForLetter(boolean parameterValue) {
        return parameterValue == true ? "&#xAC;" : "";
    }

    @Override
    protected String getSolutionInnerOperatorHtml() {
        return "&or;";
    }

    @Override
    protected String getSolutionOuterOperatorHtml() {
        return "&and;";
    }

    @Override
    protected String getExpressionOutput(String configMinTermExpression) {
        // Negate all terms in configured min-term expression (as max-term is required)
        String expression = Arrays.stream(configMinTermExpression.split(","))
                .map(item -> item.startsWith("-") ? item.substring(1) : ("-" + item))
                .collect(Collectors.joining(","));
        return expression
                .replace("-", "&#xAC;") // negation
                .replace(",", " " + getSolutionInnerOperatorHtml() + " ");
    }

    @Override
    protected Map<String, String> getLocalizedWords() {
        var localization = new HashMap<String, String>();
        localization.put("MethodName", "Maxterm");
        localization.put("CoveredElementsName", "Nullen");
        localization.put("JoinOperator", "Und-Operator (∧)");
        localization.put("NormFormTermAcc", "Konjunktive Normalform (KNF)");
        return localization;
    }
}
