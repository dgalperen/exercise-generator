# Exercise Generator development handbook

This is the development handbook of the exercise generator.

## Getting started

### Project structure

Code is checked out by cloning

  https://gitlab.com/wyd-weiss/exercise-generator.git

using your GitLab credentials.

``dev`` is the current development branch. Check this out if you are working on this project for the first time.

The directory structure looks as follows:

| Directory | Content
| ----------- | ----------- |
| backend/server | Source of Spring Boot backend server |
| backend/Postman | Postman scripts to test communication with backend |
| frontend | Source of Angular frontend |

### Install the development environment

Install the following components:

- *node* for frontend development (https://nodejs.org/en/download/). That also installs `npm', the node package manager.
- *Angular command line interface* (just type `npm install -g @angular/cli`).
- *lombok* plugin (https://projectlombok.org/) for the IDE that you use for backend development.
- *Postman* (https://www.getpostman.com/) and import Postman environment and scripts from directory `Postman/`.

#### Setup Postman

We used Postman for test communication with backend. You can setup it as follows.

- Start Postman.
- Import all files from `backend/postman` to Postman.
- Select environment `Exercise Generator local`.

#### Install PostgreSQL database (on Mac)

Exercise generator uses a PostgreSQL database. You can install PostgreSQL on MacOS X using brew:

 brew install postgres

You can start server using:

 pg_ctl -D /usr/local/var/postgres start

You can stop server again using `pg_ctl -D /usr/local/var/postgres stop`.
To start it automatically on reboot type

 brew services start postgres

Now create database user `snt` and assign privileges. Enter

 createuser --interactive --pwprompt

and answer questions

- Name of role: `exercisegenerator`
- Password: `RT3tVKuPvUi0JMHy`
- Shall the new role be a superuser?
Yes

#### Initialize database

Execute script

 init-dev-database.sh

in folder `Tools` to initialize the development database. The script creates the database and test data.

#### Start backend

How to run the backend:

- Import maven project from `backend/server/pom.xml` to your IDE.
- Start backend by running `Application` class as Java application (`backend/server/src/main/java/de/hsruhrwest/malteweiss/exercisegenerator/Application.java`).
- Now local backend is started on `http://localhost:8080`. Then call `http://localhost:8080/server/health` to test connection (local server name and version should appear).

#### Start frontend

How to run the frontend:

- Go to `frontend/`
- Enter `npm install` to install all packages (if you just checked out)
- Enter `ng serve`
- App is available at `http://localhost:4200/`.

## Conventions

### Coding

- We are working on MacOS / Unix. Newlines are *LF*. File encoding is *UTF-8*.
- Indentation is *4 spaces* (no tabs). Please configure your IDE such that hitting tab key produces 4 spaces.
- Code is commented in *English*.

### Git

We work using the following Git branches:

- `master` denotes the branch that is currently running in production. Only the project manager may touch this.
- `dev` is the current development branch for the release that we are working on. _Avoid_ working directly on this branch, but work on the features branches instead.
- All other branches are feature branches.

**Exception**: Documentary binary files (e.g., Pages documents) should always and only be commited to `dev`.

Commit comments are in *English* and *mandatory*. 

### JUnit

Every new backend feature requires a JUnit test, otherwise the feature is not considered as complete.

We aim for *85% test coverage*.