export const environment = {
  production: true,
  backendUrl: 'https://codingprof.hs-rw.de/egbackend',
  buildId: 'PROD_BUILD'
};
