import { environment } from '../environments/environment';

export class Endpoints {
  // Create backend URL
  private static createBackendUrl() {
    return environment.backendUrl;
  }

  // --- Public endpoints ---
  public static exerciseGroups() { return Endpoints.createBackendUrl() + '/api/exercise-groups'; }
  public static exercise(id:string) { return Endpoints.createBackendUrl() + '/api/exercise/' + id; }
}
