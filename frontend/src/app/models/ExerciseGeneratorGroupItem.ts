export interface ExerciseGeneratorGroupItem {
    id:string;
    name:string;
    numVariants:number;
    numViews:number;
}