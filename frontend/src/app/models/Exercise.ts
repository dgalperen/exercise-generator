export class Exercise {
    name:string;
    htmlQuestion:string;
    htmlSolution:string;
    numberOfVariants:number;
    exam:string;
}