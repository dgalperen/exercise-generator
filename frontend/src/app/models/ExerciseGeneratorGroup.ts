import { ExerciseGeneratorGroupItem } from "./ExerciseGeneratorGroupItem";

export interface ExerciseGeneratorGroup {
    exercises:ExerciseGeneratorGroupItem[];
}