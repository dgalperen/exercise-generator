import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Exercise } from 'src/app/models/Exercise';
import { ExerciseService } from '../../services/exercise.service';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.sass']
})
export class ExerciseComponent implements OnInit {

  isInitialLoading:boolean = true;
  isLoadingNewQuestion:boolean = true;
  showSolution:boolean = false;
  
  exerciseId:string;
  exerciseParamsHash:any;
  exerciseNumberOfVariants:number;
  exerciseExam:string;
  exercise:Exercise;
  questionHtml:SafeHtml;
  solutionHtml:SafeHtml;

  constructor(
    private route: ActivatedRoute,
    private exerciseService: ExerciseService,
    private domSanatizer: DomSanitizer) { 
    this.route.params.subscribe( params => {
      this.exerciseId = params['id'];
      this.loadExercise().add(() => this.isInitialLoading = false);
    });
  }

  ngOnInit(): void {
  }

  loadExercise() : Subscription {
    this.isLoadingNewQuestion = true;
    this.showSolution = false;
    return this.exerciseService.createExercise(this.exerciseId)
      .subscribe((exercise:Exercise) => {
        this.exercise = exercise;
        this.questionHtml = this.domSanatizer.bypassSecurityTrustHtml(this.exercise.htmlQuestion);
        this.solutionHtml = this.domSanatizer.bypassSecurityTrustHtml(this.exercise.htmlSolution);
        this.isLoadingNewQuestion = false;
        this.exerciseNumberOfVariants = this.exercise.numberOfVariants;
        this.exerciseExam = this.exercise.exam;
    });
  }
}
