import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SelectGeneratorComponent } from './select-generator.component';

describe('SubmissionFormatCheckComponent', () => {
  let component: SelectGeneratorComponent;
  let fixture: ComponentFixture<SelectGeneratorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
