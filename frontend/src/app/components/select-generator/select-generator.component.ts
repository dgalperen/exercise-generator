import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ExerciseService } from 'src/app/services/exercise.service';
import { ExerciseGeneratorGroup } from 'src/app/models/ExerciseGeneratorGroup';
import { ExerciseGenerator } from 'src/app/models/ExerciseGenerator';
import { ExerciseGeneratorGroupList } from 'src/app/models/ExerciseGeneratorGroupList';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-select-generator',
  templateUrl: './select-generator.component.html',
  styleUrls: ['./select-generator.component.sass']
})
export class SelectGeneratorComponent implements OnInit {

  @ViewChild('uploadSubmissionInput') uploadSubmissionInput: ElementRef;

  isInitialLoading:boolean = true;
  
  alertMessage:string;
  alertClass:string;
  totalExerciseCount:number;
  totalVariantCount:number;
  showViews:boolean = false;

  generatorGroups:ExerciseGeneratorGroup[];

  constructor(
    private route:ActivatedRoute,
    private exerciseService:ExerciseService) { 
      this.route.queryParams.subscribe( params => {
        this.showViews = params['showViews'];
      });
    }

  ngOnInit() {
    this.isInitialLoading = true;

    // Load initial configuration
    this.exerciseService.getExerciseGeneratorGroupList().subscribe((generatorGroups:ExerciseGeneratorGroupList) => {
      this.generatorGroups = generatorGroups.groups;

      this.totalExerciseCount = generatorGroups.groups
        .map(group => group.exercises.length).reduce((sum, current) => sum + current);
      this.totalVariantCount = generatorGroups.groups.flatMap(group => group.exercises)
        .map(exercise => exercise.numVariants).reduce((sum, current) => sum + current);

      this.isInitialLoading = false;
    }, error => {
      console.log(error);
      this.showError('Der Server kann aktuell nicht erreicht werden. Probieren Sie es bitte später noch einmal.');

      this.isInitialLoading = false;
    });
  }

  showError(message:string) {
    this.alertMessage = message;
    this.alertClass = 'alert-danger';
  }

  clearMessages() {
    this.alertMessage = null;
  }
}
