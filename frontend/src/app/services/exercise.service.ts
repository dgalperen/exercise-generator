import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Endpoints } from '../endpoints';
import { Observable } from 'rxjs';
import { Exercise } from '../models/Exercise';
import { ExerciseGeneratorGroupList } from '../models/ExerciseGeneratorGroupList';

@Injectable({
  providedIn: 'root'
})
export class ExerciseService {

  constructor(private http: HttpClient) { }

  /**
   * Returns generator groups
   */
  getExerciseGeneratorGroupList(): Observable<ExerciseGeneratorGroupList> {
    return this.http.get<ExerciseGeneratorGroupList>(Endpoints.exerciseGroups());
  }

  /**
   * Returns generated exam
   */
  createExercise(generatorId:string): Observable<Exercise> {
    return this.http.get<Exercise>(Endpoints.exercise(generatorId));
  }

}
