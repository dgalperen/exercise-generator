import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SelectGeneratorComponent } from './components/select-generator/select-generator.component';
import { ExerciseComponent } from './components/exercise/exercise.component';
import { TestComponent } from './components/test/test.component';

import { LOCALE_ID } from '@angular/core';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import { registerLocaleData } from '@angular/common';
import { BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ImprintComponent } from './components/imprint/imprint.component';

registerLocaleData(localeDe, localeDeExtra);

@NgModule({
  declarations: [
    AppComponent,
    SelectGeneratorComponent,
    ExerciseComponent,
    TestComponent,
    ImprintComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule
  ],
  providers: [
    [ { provide: LOCALE_ID, useValue: 'de' } ]
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
