import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SelectGeneratorComponent as SelectGeneratorComponent } from './components/select-generator/select-generator.component';
import { ExerciseComponent } from './components/exercise/exercise.component';
import { TestComponent } from './components/test/test.component';
import { ImprintComponent } from './components/imprint/imprint.component';


const routes: Routes = [
  {
    path: 'exercise/:id',
    component: ExerciseComponent
  },
  {
    path: 'e/:id',  // Short path
    redirectTo: 'exercise/:id'
  },
  {
    path: 'imprint',
    component: ImprintComponent
  },
  {
    path: 'test',
    component: TestComponent
  },
  {
    path: '**',
    component: SelectGeneratorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true } )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
